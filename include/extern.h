/*	SCCS Id: @(#)extern.h	3.3	2000/07/14	*/
/* Copyright (c) Steve Creps, 1988.				  */
/* NetHack may be freely redistributed.  See license for details. */

#ifndef EXTERN_H
#define EXTERN_H

#define E extern

#ifdef PALMOS
#define SEC1 __attribute__ ((section("S1")))
#define SEC2 __attribute__ ((section("S2")))
#define SEC3 __attribute__ ((section("S3")))
#define SEC4 __attribute__ ((section("S4")))
#define SEC5 __attribute__ ((section("S5")))
#define SEC6 __attribute__ ((section("S6")))
#define SEC7 __attribute__ ((section("S7")))
#define SEC8 __attribute__ ((section("S8")))
#define SEC9 __attribute__ ((section("S9")))
#define SEC10 __attribute__ ((section("S10")))
#define SEC11 __attribute__ ((section("S11")))
#define SEC12 __attribute__ ((section("S12")))
#define SEC13 __attribute__ ((section("S13")))
#define SEC14 __attribute__ ((section("S14")))
#define SEC15 __attribute__ ((section("S15")))
#define SEC16 __attribute__ ((section("S16")))
#define SEC17 __attribute__ ((section("S17")))
#define SEC18 __attribute__ ((section("S18")))
#define SEC19 __attribute__ ((section("S19")))
#define SEC20 __attribute__ ((section("S20")))
#define SEC21 __attribute__ ((section("S21")))
#define SEC22 __attribute__ ((section("S22")))
#define SEC23 __attribute__ ((section("S23")))
#define SEC24 __attribute__ ((section("S24")))
#define SEC25 __attribute__ ((section("S25")))
#define SEC26 __attribute__ ((section("S26")))
#define SEC27 __attribute__ ((section("S27")))
#define SEC28 __attribute__ ((section("S28")))
#define SEC29 __attribute__ ((section("S29")))
#define SEC30 __attribute__ ((section("S30")))
#define SEC31 __attribute__ ((section("S31")))
#define SEC32 __attribute__ ((section("S32")))
#define SEC33 __attribute__ ((section("S33")))
#define SEC34 __attribute__ ((section("S34")))
#define SEC35 __attribute__ ((section("S35")))
#define SEC36 __attribute__ ((section("S36")))
#define SEC37 __attribute__ ((section("S37")))
#define SEC38 __attribute__ ((section("S38")))
#define SEC39 __attribute__ ((section("S39")))
#define SEC40 __attribute__ ((section("S40")))
#define SEC41 __attribute__ ((section("S41")))
#define SEC42 __attribute__ ((section("S42")))
#define SEC43 __attribute__ ((section("S43")))
#define SEC44 __attribute__ ((section("S44")))
#define SEC45 __attribute__ ((section("S45")))
#define SEC46 __attribute__ ((section("S46")))
#define SEC47 __attribute__ ((section("S47")))
#define SEC48 __attribute__ ((section("S48")))
#define SEC49 __attribute__ ((section("S49")))
#define SEC50 __attribute__ ((section("S50")))
#define SEC51 __attribute__ ((section("S51")))
#define SEC52 __attribute__ ((section("S52")))
#define SEC53 __attribute__ ((section("S53")))
#define SEC54 __attribute__ ((section("S54")))
#define SEC55 __attribute__ ((section("S55")))
#define SEC56 __attribute__ ((section("S56")))
#define SEC57 __attribute__ ((section("S57")))
#define SEC58 __attribute__ ((section("S58")))
#define SEC59 __attribute__ ((section("S59")))
#define SEC60 __attribute__ ((section("S60")))
#define SEC61 __attribute__ ((section("S61")))
#define SEC62 __attribute__ ((section("S62")))
#define SEC63 __attribute__ ((section("S63")))

#endif

/* ### alloc.c ### */

#if 0
E long *FDECL(alloc, (unsigned int));
#endif
E char *FDECL(fmt_ptr, (const genericptr,char *)) SEC1;

/* This next pre-processor directive covers almost the entire file,
 * interrupted only occasionally to pick up specific functions as needed. */
#if !defined(MAKEDEFS_C) && !defined(LEV_LEX_C)

/* ### allmain.c ### */

E void NDECL(moveloop) SEC1;
E void NDECL(stop_occupation) SEC1;
E void NDECL(display_gamewindows) SEC1;
E void NDECL(newgame) SEC1;
E void FDECL(welcome, (BOOLEAN_P)) SEC1;

/* ### apply.c ### */

E int NDECL(doapply) SEC3;
E int NDECL(dorub) SEC60;
E int NDECL(dojump) SEC60;
E int FDECL(jump, (int)) SEC60;
E int NDECL(number_leashed) SEC60;
E void FDECL(o_unleash, (struct obj *)) SEC60;
E void FDECL(m_unleash, (struct monst *)) SEC60;
E void NDECL(unleash_all) SEC60;
E boolean NDECL(next_to_u) SEC60;
E struct obj *FDECL(get_mleash, (struct monst *)) SEC60;
E void FDECL(check_leash, (XCHAR_P,XCHAR_P)) SEC60;
E boolean FDECL(wield_tool, (struct obj *)) SEC60;
E boolean FDECL(um_dist, (XCHAR_P,XCHAR_P,XCHAR_P)) SEC60;
E boolean FDECL(snuff_candle, (struct obj *)) SEC60;
E boolean FDECL(snuff_lit, (struct obj *)) SEC60;
E void FDECL(use_unicorn_horn, (struct obj *)) SEC60;
E boolean FDECL(tinnable, (struct obj *)) SEC2;
E void NDECL(reset_trapset) SEC2;
E void FDECL(fig_transform, (genericptr_t, long)) SEC2;

/* ### artifact.c ### */

E void NDECL(init_artifacts) SEC3;
E void FDECL(save_artifacts, (int)) SEC3;
E void FDECL(restore_artifacts, (int)) SEC3;
E const char *FDECL(artiname, (int)) SEC3;
E struct obj *FDECL(mk_artifact, (struct obj *,ALIGNTYP_P)) SEC3;
E const char *FDECL(artifact_name, (const char *,short *)) SEC3;
E boolean FDECL(exist_artifact, (int,const char *)) SEC3;
E void FDECL(artifact_exists, (struct obj *,const char *,BOOLEAN_P)) SEC3;
E int NDECL(nartifact_exist) SEC3;
E boolean FDECL(spec_ability, (struct obj *,unsigned long)) SEC3;
E boolean FDECL(restrict_name, (struct obj *,const char *)) SEC3;
E boolean FDECL(defends, (int,struct obj *)) SEC3;
E boolean FDECL(protects, (int,struct obj *)) SEC3;
E void FDECL(set_artifact_intrinsic, (struct obj *,BOOLEAN_P,long)) SEC3;
E int FDECL(touch_artifact, (struct obj *,struct monst *)) SEC3;
E int FDECL(spec_abon, (struct obj *,struct monst *)) SEC3;
E int FDECL(spec_dbon, (struct obj *,struct monst *,int)) SEC3;
E void FDECL(discover_artifact, (XCHAR_P)) SEC3;
E boolean FDECL(undiscovered_artifact, (XCHAR_P)) SEC3;
E int FDECL(disp_artifact_discoveries, (winid)) SEC3;
E boolean FDECL(artifact_hit, (struct monst *,struct monst *,
				struct obj *,int *,int)) SEC3;
E int NDECL(doinvoke) SEC3;
E void FDECL(arti_speak, (struct obj *)) SEC3;
E long FDECL(spec_m2, (struct obj *)) SEC3;

/* ### attrib.c ### */

E boolean FDECL(adjattrib, (int,int,int)) SEC3;
E void FDECL(change_luck, (SCHAR_P)) SEC3;
E int FDECL(stone_luck, (BOOLEAN_P)) SEC3;
E void NDECL(set_moreluck) SEC3;
E void FDECL(gainstr, (struct obj *,int)) SEC3;
E void FDECL(losestr, (int)) SEC3;
E void NDECL(restore_attrib) SEC3;
E void FDECL(exercise, (int,BOOLEAN_P)) SEC3;
E void NDECL(exerchk) SEC3;
E void NDECL(reset_attribute_clock) SEC3;
E void FDECL(init_attr, (int)) SEC3;
E void NDECL(redist_attr) SEC3;
E void FDECL(adjabil, (int,int)) SEC3;
E int NDECL(newhp) SEC3;
E schar FDECL(acurr, (int)) SEC3;
E schar NDECL(acurrstr) SEC3;
E void FDECL(adjalign, (int)) SEC3;

/* ### ball.c ### */

E void NDECL(ballfall) SEC3;
E void NDECL(placebc) SEC3;
E void NDECL(unplacebc) SEC3;
E void FDECL(set_bc, (int)) SEC3;
E void FDECL(move_bc, (int,int,XCHAR_P,XCHAR_P,XCHAR_P,XCHAR_P)) SEC3;
E boolean FDECL(drag_ball, (XCHAR_P,XCHAR_P,
		    int *,xchar *,xchar *,xchar *,xchar *, boolean *)) SEC3;
E void FDECL(drop_ball, (XCHAR_P,XCHAR_P)) SEC3;
E void NDECL(drag_down) SEC3;

/* ### bones.c ### */

E boolean NDECL(can_make_bones) SEC4;
E void FDECL(savebones, (struct obj *)) SEC4;
E int NDECL(getbones) SEC4;

/* ### botl.c ### */

E int FDECL(xlev_to_rank, (int)) SEC4;
E int FDECL(title_to_mon, (const char *,int *,int *)) SEC4;
E void NDECL(max_rank_sz) SEC4;
#ifdef SCORE_ON_BOTL
E long NDECL(botl_score) SEC4;
#endif
E int FDECL(describe_level, (char *)) SEC4;
E const char *FDECL(rank_of, (int,SHORT_P,BOOLEAN_P)) SEC4;
E void NDECL(bot) SEC4;

/* ### cmd.c ### */

#ifdef USE_TRAMPOLI
E int NDECL(doextcmd) SEC5;
E int NDECL(domonability) SEC5;
E int NDECL(doprev_message) SEC5;
E int NDECL(timed_occupation) SEC5;
E int NDECL(wiz_attributes) SEC5;
E int NDECL(enter_explore_mode) SEC5;
# ifdef WIZARD
E int NDECL(wiz_detect) SEC5;
E int NDECL(wiz_genesis) SEC5;
E int NDECL(wiz_identify) SEC5;
E int NDECL(wiz_level_tele) SEC5;
E int NDECL(wiz_map) SEC5;
E int NDECL(wiz_where) SEC5;
E int NDECL(wiz_wish) SEC5;
# endif /* WIZARD */
#endif /* USE_TRAMPOLI */
E void NDECL(reset_occupations) SEC5;
E void FDECL(set_occupation, (int (*)(void),const char *,int)) SEC5;
#ifdef REDO
E char NDECL(pgetchar) SEC5;
E void FDECL(pushch, (CHAR_P)) SEC5;
E void FDECL(savech, (CHAR_P)) SEC5;
#endif
#ifdef WIZARD
E void NDECL(add_debug_extended_commands) SEC5;
#endif /* WIZARD */
E void FDECL(rhack, (char *)) SEC5;
E int NDECL(doextlist) SEC5;
E int NDECL(extcmd_via_menu) SEC5;
E void FDECL(enlightenment, (int)) SEC5;
E void FDECL(show_conduct, (int)) SEC5;
E int FDECL(xytod, (SCHAR_P,SCHAR_P)) SEC5;
E void FDECL(dtoxy, (coord *,int)) SEC5;
E int FDECL(movecmd, (CHAR_P)) SEC5;
E int FDECL(getdir, (const char *)) SEC5;
E void NDECL(confdir) SEC5;
E int FDECL(isok, (int,int)) SEC5;
E int FDECL(click_to_cmd, (int,int,int)) SEC5;
E char NDECL(readchar) SEC5;
#ifdef WIZARD
E void NDECL(sanity_check) SEC5;
#endif

/* ### dbridge.c ### */

E boolean FDECL(is_pool, (int,int)) SEC6;
E boolean FDECL(is_lava, (int,int)) SEC6;
E boolean FDECL(is_ice, (int,int)) SEC6;
E int FDECL(is_drawbridge_wall, (int,int)) SEC6;
E boolean FDECL(is_db_wall, (int,int)) SEC6;
E boolean FDECL(find_drawbridge, (int *,int*)) SEC6;
E boolean FDECL(create_drawbridge, (int,int,int,BOOLEAN_P)) SEC6;
E void FDECL(open_drawbridge, (int,int)) SEC6;
E void FDECL(close_drawbridge, (int,int)) SEC6;
E void FDECL(destroy_drawbridge, (int,int)) SEC6;

/* ### decl.c ### */

E void NDECL(decl_init) SEC6;

/* ### detect.c ### */

E struct obj *FDECL(o_in, (struct obj*,CHAR_P)) SEC6;
E int FDECL(gold_detect, (struct obj *)) SEC6;
E int FDECL(food_detect, (struct obj *)) SEC6;
E int FDECL(object_detect, (struct obj *,int)) SEC6;
E int FDECL(monster_detect, (struct obj *,int)) SEC6;
E int FDECL(trap_detect, (struct obj *)) SEC6;
E const char *FDECL(level_distance, (d_level *)) SEC6;
E void FDECL(use_crystal_ball, (struct obj *)) SEC6;
E void NDECL(do_mapping) SEC6;
E void NDECL(do_vicinity_map) SEC6;
E void FDECL(cvt_sdoor_to_door, (struct rm *)) SEC6;
#ifdef USE_TRAMPOLI
E void FDECL(findone, (int,int,genericptr_t)) SEC6;
E void FDECL(openone, (int,int,genericptr_t)) SEC6;
#endif
E int NDECL(findit) SEC6;
E int NDECL(openit) SEC6;
E void FDECL(find_trap, (struct trap *)) SEC6;
E int FDECL(dosearch0, (int)) SEC6;
E int NDECL(dosearch) SEC6;
E void NDECL(sokoban_detect) SEC6;

/* ### dig.c ### */

#ifdef USE_TRAMPOLI
E int NDECL(dig) SEC7;
#endif
E int NDECL(holetime) SEC7;
E boolean FDECL(dig_check, (struct monst *, BOOLEAN_P, int, int)) SEC7;
E void FDECL(digactualhole, (int,int,struct monst *,int)) SEC7;
E boolean FDECL(dighole, (BOOLEAN_P)) SEC7;
E int FDECL(use_pick_axe, (struct obj *)) SEC7;
E boolean FDECL(mdig_tunnel, (struct monst *)) SEC7;
E void NDECL(zap_dig) SEC7;
E struct obj *FDECL(bury_an_obj, (struct obj *)) SEC7;
E void FDECL(bury_objs, (int,int)) SEC7;
E void FDECL(unearth_objs, (int,int)) SEC7;
E void FDECL(rot_organic, (genericptr_t, long)) SEC7;
E void FDECL(rot_corpse, (genericptr_t, long)) SEC7;
#if 0
E void FDECL(bury_monst, (struct monst *));
E void NDECL(bury_you);
E void NDECL(unearth_you);
E void NDECL(escape_tomb);
E void FDECL(bury_obj, (struct obj *));
#endif

/* ### display.c ### */

#ifdef INVISIBLE_OBJECTS
E struct obj * FDECL(vobj_at, (XCHAR_P,XCHAR_P)) SEC8;
#endif /* INVISIBLE_OBJECTS */
E void FDECL(magic_map_background, (XCHAR_P,XCHAR_P,int)) SEC8;
E void FDECL(map_background, (XCHAR_P,XCHAR_P,int)) SEC8;
E void FDECL(map_trap, (struct trap *,int)) SEC8;
E void FDECL(map_object, (struct obj *,int)) SEC8;
E void FDECL(map_invisible, (XCHAR_P,XCHAR_P)) SEC8;
E void FDECL(unmap_object, (int,int)) SEC8;
E void FDECL(map_location, (int,int,int)) SEC8;
E void FDECL(feel_location, (XCHAR_P,XCHAR_P)) SEC8;
E void FDECL(newsym, (int,int)) SEC8;
E void FDECL(shieldeff, (XCHAR_P,XCHAR_P)) SEC8;
E void FDECL(tmp_at, (int,int)) SEC8;
E void FDECL(swallowed, (int)) SEC8;
E void FDECL(under_ground, (int)) SEC8;
E void FDECL(under_water, (int)) SEC8;
E void NDECL(see_monsters) SEC8;
E void NDECL(set_mimic_blocking) SEC8;
E void NDECL(see_objects) SEC8;
E void NDECL(see_traps) SEC8;
E void NDECL(curs_on_u) SEC8;
E int NDECL(doredraw) SEC8;
E void NDECL(docrt) SEC8;
E void FDECL(show_glyph, (int,int,int)) SEC8;
E void NDECL(clear_glyph_buffer) SEC8;
E void FDECL(row_refresh, (int,int,int)) SEC8;
E void NDECL(cls) SEC8;
E void FDECL(flush_screen, (int)) SEC8;
E int FDECL(back_to_glyph, (XCHAR_P,XCHAR_P)) SEC8;
E int FDECL(zapdir_to_glyph, (int,int,int)) SEC8;
E int FDECL(glyph_at, (XCHAR_P,XCHAR_P)) SEC8;
E void NDECL(set_wall_state) SEC8;

/* ### do.c ### */

#ifdef USE_TRAMPOLI
E int FDECL(drop, (struct obj *)) SEC9;
E int NDECL(wipeoff) SEC9;
#endif
E int NDECL(dodrop) SEC9;
E boolean FDECL(boulder_hits_pool, (struct obj *,int,int,BOOLEAN_P)) SEC9;
E boolean FDECL(flooreffects, (struct obj *,int,int,const char *)) SEC9;
E void FDECL(doaltarobj, (struct obj *)) SEC9;
E boolean FDECL(canletgo, (struct obj *,const char *)) SEC9;
E void FDECL(dropx, (struct obj *)) SEC9;
E void FDECL(dropy, (struct obj *)) SEC9;
E int NDECL(doddrop) SEC9;
E int NDECL(dodown) SEC9;
E int NDECL(doup) SEC9;
#ifdef INSURANCE
E void NDECL(save_currentstate) SEC9;
#endif
E void FDECL(goto_level, (d_level *,BOOLEAN_P,BOOLEAN_P,BOOLEAN_P)) SEC9;
E void FDECL(schedule_goto, (d_level *,BOOLEAN_P,BOOLEAN_P,int,
			     const char *,const char *)) SEC9;
E void NDECL(deferred_goto) SEC9;
E boolean FDECL(revive_corpse, (struct obj *)) SEC9;
E void FDECL(revive_mon, (genericptr_t, long)) SEC9;
E int NDECL(donull) SEC9;
E int NDECL(dowipe) SEC9;
E void FDECL(set_wounded_legs, (long,int)) SEC9;
E void NDECL(heal_legs) SEC9;

/* ### do_name.c ### */

E int FDECL(getpos, (coord *,BOOLEAN_P,const char *)) SEC10;
E struct monst *FDECL(christen_monst, (struct monst *,const char *)) SEC10;
E int NDECL(do_mname) SEC10;
E struct obj *FDECL(oname, (struct obj *,const char *)) SEC10;
E int NDECL(ddocall) SEC10;
E void FDECL(docall, (struct obj *)) SEC10;
E const char *NDECL(rndghostname) SEC10;
E char *FDECL(x_monnam, (struct monst *,int,const char *,int,BOOLEAN_P)) SEC10;
E char *FDECL(l_monnam, (struct monst *)) SEC10;
E char *FDECL(mon_nam, (struct monst *)) SEC10;
E char *FDECL(noit_mon_nam, (struct monst *)) SEC10;
E char *FDECL(Monnam, (struct monst *)) SEC10;
E char *FDECL(noit_Monnam, (struct monst *)) SEC10;
E char *FDECL(m_monnam, (struct monst *)) SEC10;
E char *FDECL(y_monnam, (struct monst *)) SEC10;
E char *FDECL(Adjmonnam, (struct monst *,const char *)) SEC10;
E char *FDECL(Amonnam, (struct monst *)) SEC10;
E char *FDECL(a_monnam, (struct monst *)) SEC10;
E const char *NDECL(rndmonnam) SEC10;
E const char *FDECL(hcolor, (const char *)) SEC10;
E char *FDECL(self_pronoun, (const char *,const char *)) SEC10;
#ifdef REINCARNATION
E const char *NDECL(roguename) SEC10;
#endif
E struct obj *FDECL(realloc_obj,
		(struct obj *, int, genericptr_t, int, const char *)) SEC10;
E char *FDECL(coyotename, (char *)) SEC10;

/* ### do_wear.c ### */

#ifdef USE_TRAMPOLI
E int NDECL(Armor_on) SEC11;
E int NDECL(Boots_on) SEC11;
E int NDECL(Gloves_on) SEC11;
E int NDECL(Helmet_on) SEC11;
E int FDECL(select_off, (struct obj *)) SEC11;
E int NDECL(take_off) SEC11;
#endif
E void FDECL(off_msg, (struct obj *)) SEC11;
E void NDECL(set_wear) SEC11;
E boolean FDECL(donning, (struct obj *)) SEC11;
E void NDECL(cancel_don) SEC11;
E int NDECL(Armor_off) SEC11;
E int NDECL(Armor_gone) SEC11;
E int NDECL(Helmet_off) SEC11;
E int NDECL(Gloves_off) SEC11;
E int NDECL(Boots_off) SEC11;
E int NDECL(Cloak_off) SEC11;
E int NDECL(Shield_off) SEC11;
E void NDECL(Amulet_off) SEC11;
E void FDECL(Ring_on, (struct obj *)) SEC11;
E void FDECL(Ring_off, (struct obj *)) SEC11;
E void FDECL(Ring_gone, (struct obj *)) SEC11;
E void FDECL(Blindf_on, (struct obj *)) SEC11;
E void FDECL(Blindf_off, (struct obj *)) SEC11;
E int NDECL(dotakeoff) SEC11;
E int NDECL(doremring) SEC11;
E int FDECL(cursed, (struct obj *)) SEC11;
E int FDECL(armoroff, (struct obj *)) SEC11;
E int FDECL(canwearobj, (struct obj *, long *, BOOLEAN_P)) SEC11;
E int NDECL(dowear) SEC11;
E int NDECL(doputon) SEC11;
E void NDECL(find_ac) SEC11;
E void NDECL(glibr) SEC11;
E struct obj *FDECL(some_armor,(struct monst *)) SEC11;
E void FDECL(erode_armor, (struct monst *,BOOLEAN_P)) SEC11;
E void NDECL(reset_remarm) SEC11;
E int NDECL(doddoremarm) SEC11;
E int FDECL(destroy_arm, (struct obj *)) SEC11;
E void FDECL(adj_abon, (struct obj *,SCHAR_P)) SEC11;

/* ### dog.c ### */

E void FDECL(initedog, (struct monst *)) SEC12;
E struct monst *FDECL(make_familiar, (struct obj *,XCHAR_P,XCHAR_P,BOOLEAN_P)) SEC12;;
E struct monst *NDECL(makedog) SEC12;;
E void NDECL(update_mlstmv) SEC12;;
E void NDECL(losedogs) SEC12;;
E void FDECL(mon_arrive, (struct monst *,BOOLEAN_P)) SEC12;;
E void FDECL(mon_catchup_elapsed_time, (struct monst *,long)) SEC12;;
E void FDECL(keepdogs, (BOOLEAN_P)) SEC12;;
E void FDECL(migrate_to_level, (struct monst *,XCHAR_P,XCHAR_P,coord *)) SEC12;;
E int FDECL(dogfood, (struct monst *,struct obj *)) SEC12;;
E struct monst *FDECL(tamedog, (struct monst *,struct obj *)) SEC12;;
E void FDECL(abuse_dog, (struct monst *)) SEC12;;
E void FDECL(wary_dog, (struct monst *, BOOLEAN_P)) SEC12;;

/* ### dogmove.c ### */

E int FDECL(dog_nutrition, (struct monst *,struct obj *)) SEC12;;
E int FDECL(dog_eat, (struct monst *,struct obj *,int,int,BOOLEAN_P)) SEC12;;
E int FDECL(dog_move, (struct monst *,int)) SEC12;;
#ifdef USE_TRAMPOLI
E void FDECL(wantdoor, (int,int,genericptr_t)) SEC12;;
#endif

/* ### dokick.c ### */

E boolean FDECL(ghitm, (struct monst *,struct obj *)) SEC13;
E int NDECL(dokick) SEC13;
E boolean FDECL(ship_object, (struct obj *,XCHAR_P,XCHAR_P,BOOLEAN_P)) SEC13;
E void NDECL(obj_delivery) SEC13;
E schar FDECL(down_gate, (XCHAR_P,XCHAR_P)) SEC13;
E void FDECL(impact_drop, (struct obj *,XCHAR_P,XCHAR_P,XCHAR_P)) SEC13;

/* ### dothrow.c ### */

E int NDECL(dothrow) SEC14;
E int NDECL(dofire) SEC14;
E void FDECL(hitfloor, (struct obj *)) SEC14;
E void FDECL(hurtle, (int,int,int,BOOLEAN_P)) SEC14;
E void FDECL(throwit, (struct obj *,long)) SEC14;
E int FDECL(omon_adj, (struct monst *,struct obj *,BOOLEAN_P)) SEC14;
E int FDECL(thitmonst, (struct monst *,struct obj *)) SEC14;
E int FDECL(hero_breaks, (struct obj *,XCHAR_P,XCHAR_P,BOOLEAN_P)) SEC14;
E int FDECL(breaks, (struct obj *,XCHAR_P,XCHAR_P)) SEC14;
E boolean FDECL(breaktest, (struct obj *)) SEC14;
E boolean FDECL(walk_path, (coord *, coord *, boolean (*)(genericptr_t,int,int), genericptr_t)) SEC14;
E boolean FDECL(hurtle_step, (genericptr_t, int, int)) SEC14;

/* ### drawing.c ### */
#endif /* !MAKEDEFS_C && !LEV_LEX_C */
E int FDECL(def_char_to_objclass, (CHAR_P)) SEC14;
E int FDECL(def_char_to_monclass, (CHAR_P)) SEC14;
#if !defined(MAKEDEFS_C) && !defined(LEV_LEX_C)
E void FDECL(assign_graphics, (uchar *,int,int,int)) SEC14;
E void FDECL(switch_graphics, (int)) SEC14;
#ifdef REINCARNATION
E void FDECL(assign_rogue_graphics, (BOOLEAN_P)) SEC14;
#endif

/* ### dungeon.c ### */

E void FDECL(save_dungeon, (int,BOOLEAN_P,BOOLEAN_P)) SEC15;
E void FDECL(restore_dungeon, (int)) SEC15;
E void FDECL(insert_branch, (branch *,BOOLEAN_P)) SEC15;
E void NDECL(init_dungeons) SEC15;
E s_level *FDECL(find_level, (const char *)) SEC15;
E s_level *FDECL(Is_special, (d_level *)) SEC15;
E branch *FDECL(Is_branchlev, (d_level *)) SEC15;
E xchar FDECL(ledger_no, (d_level *)) SEC15;
E xchar NDECL(maxledgerno) SEC15;
E schar FDECL(depth, (d_level *)) SEC15;
E xchar FDECL(dunlev, (d_level *)) SEC15;
E xchar FDECL(dunlevs_in_dungeon, (d_level *)) SEC15;
E xchar FDECL(ledger_to_dnum, (XCHAR_P)) SEC15;
E xchar FDECL(ledger_to_dlev, (XCHAR_P)) SEC15;
E xchar FDECL(deepest_lev_reached, (BOOLEAN_P)) SEC15;
E boolean FDECL(on_level, (d_level *,d_level *)) SEC15;
E void FDECL(next_level, (BOOLEAN_P)) SEC15;
E void FDECL(prev_level, (BOOLEAN_P)) SEC15;
E void FDECL(u_on_newpos, (int,int)) SEC15;
E void NDECL(u_on_sstairs) SEC15;
E void NDECL(u_on_upstairs) SEC15;
E void NDECL(u_on_dnstairs) SEC15;
E boolean FDECL(On_stairs, (XCHAR_P,XCHAR_P)) SEC15;
E void FDECL(get_level, (d_level *,int)) SEC15;
E boolean FDECL(Is_botlevel, (d_level *)) SEC15;
E boolean FDECL(Can_fall_thru, (d_level *)) SEC15;
E boolean FDECL(Can_dig_down, (d_level *)) SEC15;
E boolean FDECL(Can_rise_up, (int,int,d_level *)) SEC15;
E boolean FDECL(In_quest, (d_level *)) SEC15;
E boolean FDECL(In_mines, (d_level *)) SEC15;
E branch *FDECL(dungeon_branch, (const char *)) SEC15;
E boolean FDECL(at_dgn_entrance, (const char *)) SEC15;
E boolean FDECL(In_hell, (d_level *)) SEC15;
E boolean FDECL(In_V_tower, (d_level *)) SEC15;
E boolean FDECL(On_W_tower_level, (d_level *)) SEC15;
E boolean FDECL(In_W_tower, (int,int,d_level *)) SEC15;
E void FDECL(find_hell, (d_level *)) SEC15;
E void FDECL(goto_hell, (BOOLEAN_P,BOOLEAN_P)) SEC15;
E void FDECL(assign_level, (d_level *,d_level *)) SEC15;
E void FDECL(assign_rnd_level, (d_level *,d_level *,int)) SEC15;
E int FDECL(induced_align, (int)) SEC15;
E boolean FDECL(Invocation_lev, (d_level *)) SEC15;
E xchar NDECL(level_difficulty) SEC15;
E schar FDECL(lev_by_name, (const char *)) SEC15;
#ifdef WIZARD
E void NDECL(print_dungeon) SEC15;
#endif

/* ### eat.c ### */

#ifdef USE_TRAMPOLI
E int NDECL(eatmdone) SEC16;
E int NDECL(eatfood) SEC16;
E int NDECL(opentin) SEC16;
E int NDECL(unfaint) SEC16;
#endif
E boolean FDECL(is_edible, (struct obj *)) SEC16;
E void NDECL(init_uhunger) SEC16;
E int NDECL(Hear_again) SEC16;
E void NDECL(reset_eat) SEC16;
E int NDECL(doeat) SEC16;
E void NDECL(gethungry) SEC16;
E void FDECL(morehungry, (int)) SEC16;
E void FDECL(lesshungry, (int)) SEC16;
E boolean NDECL(is_fainted) SEC16;
E void NDECL(reset_faint) SEC16;
E void NDECL(violated_vegetarian) SEC16;
#if 0
E void NDECL(sync_hunger);
#endif
E void FDECL(newuhs, (BOOLEAN_P)) SEC16;
E struct obj *FDECL(floorfood, (const char *,int)) SEC16;
E void NDECL(vomit) SEC16;
E int FDECL(eaten_stat, (int,struct obj *)) SEC16;
E void FDECL(food_disappears, (struct obj *)) SEC16;
E void FDECL(food_substitution, (struct obj *,struct obj *)) SEC16;
E void NDECL(fix_petrification) SEC16;

/* ### end.c ### */

E void FDECL(done1, (int)) SEC17;
E int NDECL(done2) SEC17;
#ifdef USE_TRAMPOLI
E void FDECL(done_intr, (int)) SEC17;
#endif
E void FDECL(done_in_by, (struct monst *)) SEC17;
#endif /* !MAKEDEFS_C && !LEV_LEX_C */
E void VDECL(panic, (const char *,...)) PRINTF_F(1,2) SEC17;
#if !defined(MAKEDEFS_C) && !defined(LEV_LEX_C)
E void FDECL(done, (int)) SEC17;
E void FDECL(container_contents, (struct obj *,BOOLEAN_P,BOOLEAN_P)) SEC17;
E void FDECL(terminate, (int)) SEC17;
E int NDECL(num_genocides) SEC17;

/* ### engrave.c ### */

E char *FDECL(random_engraving, (char *)) SEC17;
E void FDECL(wipeout_text, (char *,int,unsigned)) SEC17;
E boolean NDECL(can_reach_floor) SEC17;
E const char *FDECL(surface, (int,int)) SEC17;
E const char *FDECL(ceiling, (int,int)) SEC17;
E struct engr *FDECL(engr_at, (XCHAR_P,XCHAR_P)) SEC17;
#ifdef ELBERETH
E int FDECL(sengr_at, (const char *,XCHAR_P,XCHAR_P)) SEC17;
#endif
E void FDECL(u_wipe_engr, (int)) SEC17;
E void FDECL(wipe_engr_at, (XCHAR_P,XCHAR_P,XCHAR_P)) SEC17;
E void FDECL(read_engr_at, (int,int)) SEC17;
E void FDECL(make_engr_at, (int,int,const char *,long,XCHAR_P)) SEC17;
E void FDECL(del_engr_at, (int,int)) SEC17;
E int NDECL(freehand) SEC17;
E int NDECL(doengrave) SEC17;
E void FDECL(save_engravings, (int,int)) SEC17;
E void FDECL(rest_engravings, (int)) SEC17;
E void FDECL(del_engr, (struct engr *)) SEC17;
E void FDECL(rloc_engr, (struct engr *)) SEC17;
E void FDECL(make_grave, (int,int,const char *)) SEC17;

/* ### exper.c ### */

E int FDECL(experience, (struct monst *,int)) SEC17;
E void FDECL(more_experienced, (int,int)) SEC17;
E void FDECL(losexp, (const char *)) SEC17;
E void NDECL(newexplevel) SEC17;
E void FDECL(pluslvl, (BOOLEAN_P)) SEC17;
E long NDECL(rndexp) SEC17;

/* ### explode.c ### */

E void FDECL(explode, (int,int,int,int,CHAR_P)) SEC18;
E void FDECL(scatter, (int, int, int, unsigned int, struct obj *)) SEC18;
E void FDECL(splatter_burning_oil, (int, int)) SEC18;

/* ### extralev.c ### */

#ifdef REINCARNATION
E void NDECL(makeroguerooms) SEC18;
E void FDECL(corr, (int,int)) SEC18;
E void NDECL(makerogueghost) SEC18;
#endif

/* ### files.c ### */

E const char *FDECL(fqname, (const char *, int, int)) SEC18;
E FILE *FDECL(fopen_datafile, (const char *,const char *,BOOLEAN_P)) SEC18;
E boolean FDECL(uptodate, (int,const char *)) SEC18;
E void FDECL(store_version, (int)) SEC18;
#ifdef MFLOPPY
E void NDECL(set_lock_and_bones) SEC18;
#endif
E void FDECL(set_levelfile_name, (char *,int)) SEC18;
E int FDECL(create_levelfile, (int)) SEC18;
E int FDECL(open_levelfile, (int)) SEC18;
E void FDECL(delete_levelfile, (int)) SEC18;
E void NDECL(clearlocks) SEC18;
E int FDECL(create_bonesfile, (d_level*,char **)) SEC18;
#ifdef MFLOPPY
E void NDECL(cancel_bonesfile) SEC18;
#endif
E void FDECL(commit_bonesfile, (d_level *)) SEC18;
E int FDECL(open_bonesfile, (d_level*,char **)) SEC18;
E int FDECL(delete_bonesfile, (d_level*)) SEC18;
E void NDECL(compress_bonesfile) SEC18;
E void NDECL(set_savefile_name) SEC18;
#ifdef INSURANCE
E void FDECL(save_savefile_name, (int)) SEC18;
#endif
#if defined(WIZARD) && !defined(MICRO)
E void NDECL(set_error_savefile) SEC18;
#endif
E int NDECL(create_savefile) SEC18;
E int NDECL(open_savefile) SEC18;
E int NDECL(delete_savefile) SEC18;
E int NDECL(restore_saved_game) SEC18;
E void FDECL(compress, (const char *)) SEC18;
E void FDECL(uncompress, (const char *)) SEC18;
E boolean FDECL(lock_file, (const char *,int,int)) SEC18;
E void FDECL(unlock_file, (const char *)) SEC18;
E void FDECL(read_config_file, (const char *)) SEC18;
E void FDECL(check_recordfile, (const char *)) SEC18;

/* ### fountain.c ### */

E void FDECL(floating_above, (const char *)) SEC18;
E void FDECL(dogushforth, (int)) SEC18;
# ifdef USE_TRAMPOLI
E void FDECL(gush, (int,int,genericptr_t)) SEC18;
# endif
E void FDECL(dryup, (XCHAR_P,XCHAR_P, BOOLEAN_P)) SEC18;
E void NDECL(drinkfountain) SEC18;
E void FDECL(dipfountain, (struct obj *)) SEC18;
#ifdef SINKS
E void FDECL(breaksink, (int,int)) SEC18;
E void NDECL(drinksink) SEC18;
#endif

/* ### hack.c ### */

E boolean FDECL(revive_nasty, (int,int,const char*)) SEC19;
E void FDECL(movobj, (struct obj *,XCHAR_P,XCHAR_P)) SEC19;
E boolean FDECL(may_dig, (XCHAR_P,XCHAR_P)) SEC19;
E boolean FDECL(may_passwall, (XCHAR_P,XCHAR_P)) SEC19;
E boolean FDECL(bad_rock, (struct permonst *,XCHAR_P,XCHAR_P)) SEC19;
E boolean FDECL(invocation_pos, (XCHAR_P,XCHAR_P)) SEC19;
E void NDECL(domove) SEC19;
E void NDECL(invocation_message) SEC19;
E void FDECL(spoteffects, (BOOLEAN_P)) SEC19;
E char *FDECL(in_rooms, (XCHAR_P,XCHAR_P,int)) SEC19;
E void FDECL(check_special_room, (BOOLEAN_P)) SEC19;
E int NDECL(dopickup) SEC19;
E void NDECL(lookaround) SEC19;
E int NDECL(monster_nearby) SEC19;
E void FDECL(nomul, (int)) SEC19;
E void FDECL(unmul, (const char *)) SEC19;
E void FDECL(losehp, (int,const char *,BOOLEAN_P)) SEC19;
E int NDECL(weight_cap) SEC19;
E int NDECL(inv_weight) SEC19;
E int NDECL(near_capacity) SEC19;
E int FDECL(calc_capacity, (int)) SEC19;
E int NDECL(max_capacity) SEC19;
E boolean FDECL(check_capacity, (const char *)) SEC19;
E int NDECL(inv_cnt) SEC19;

/* ### hacklib.c ### */

E boolean FDECL(digit, (CHAR_P)) SEC20;
E boolean FDECL(letter, (CHAR_P)) SEC20;
E char FDECL(highc, (CHAR_P)) SEC20;
E char FDECL(lowc, (CHAR_P)) SEC20;
E char *FDECL(lcase, (char *)) SEC20;
E char *FDECL(mungspaces, (char *)) SEC20;
E char *FDECL(eos, (char *)) SEC20;
E char *FDECL(s_suffix, (const char *)) SEC20;
E char *FDECL(xcrypt, (const char *,char *)) SEC20;
E boolean FDECL(onlyspace, (const char *)) SEC20;
E char *FDECL(tabexpand, (char *)) SEC20;
E char *FDECL(visctrl, (CHAR_P)) SEC20;
E const char *FDECL(ordin, (int)) SEC20;
E char *FDECL(sitoa, (int)) SEC20;
E int FDECL(sgn, (int)) SEC20;
E int FDECL(rounddiv, (long,int)) SEC20;
E int FDECL(dist2, (int,int,int,int)) SEC20;
E int FDECL(distmin, (int,int,int,int)) SEC20;
E boolean FDECL(online2, (int,int,int,int)) SEC20;
E boolean FDECL(pmatch, (const char *,const char *)) SEC20;
#ifndef STRNCMPI
E int FDECL(strncmpi, (const char *,const char *,int)) SEC20;
#endif
#ifndef STRSTRI
E char *FDECL(strstri, (const char *,const char *)) SEC20;
#endif
E boolean FDECL(fuzzymatch, (const char *,const char *,const char *,BOOLEAN_P)) SEC20;
E void NDECL(setrandom) SEC20;
E int NDECL(getyear) SEC20;
#if 0
E char *FDECL(yymmdd, (time_t));
#endif
E long FDECL(yyyymmdd, (time_t)) SEC20;
E int NDECL(phase_of_the_moon) SEC20;
E boolean NDECL(friday_13th) SEC20;
E int NDECL(night) SEC20;
E int NDECL(midnight) SEC20;

/* ### invent.c ### */

E void FDECL(assigninvlet, (struct obj *)) SEC20;
E struct obj *FDECL(merge_choice, (struct obj *,struct obj *)) SEC20;
E int FDECL(merged, (struct obj **,struct obj **)) SEC20;
#ifdef USE_TRAMPOLI
E int FDECL(ckunpaid, (struct obj *)) SEC20;
#endif
E void FDECL(addinv_core1, (struct obj *)) SEC20;
E void FDECL(addinv_core2, (struct obj *)) SEC20;
E struct obj *FDECL(addinv, (struct obj *)) SEC20;
E struct obj *FDECL(hold_another_object,
			(struct obj *,const char *,const char *,const char *)) SEC20;
E void FDECL(useupall, (struct obj *)) SEC20;
E void FDECL(useup, (struct obj *)) SEC20;
E void FDECL(freeinv_core, (struct obj *)) SEC20;
E void FDECL(freeinv, (struct obj *)) SEC20;
E void FDECL(delallobj, (int,int)) SEC20;
E void FDECL(delobj, (struct obj *)) SEC20;
E struct obj *FDECL(sobj_at, (int,int,int)) SEC20;
E struct obj *FDECL(carrying, (int)) SEC20;
E boolean NDECL(have_lizard) SEC20;
E struct obj *FDECL(o_on, (unsigned int,struct obj *)) SEC20;
E boolean FDECL(obj_here, (struct obj *,int,int)) SEC20;
E boolean NDECL(wearing_armor) SEC20;
E boolean FDECL(is_worn, (struct obj *)) SEC20;
E struct obj *FDECL(g_at, (int,int)) SEC20;
E struct obj *FDECL(mkgoldobj, (long)) SEC20;
E struct obj *FDECL(getobj, (const char *,const char *)) SEC20;
E int FDECL(ggetobj, (const char *,int (*)(OBJ_P),int,BOOLEAN_P)) SEC20;
E void FDECL(fully_identify_obj, (struct obj *)) SEC20;
E int FDECL(identify, (struct obj *)) SEC20;
E void FDECL(identify_pack, (int)) SEC20;
E int FDECL(askchain, (struct obj **,const char *,int,int (*)(OBJ_P),
			int (*)(OBJ_P),int,const char *)) SEC20;
E void FDECL(prinv, (const char *,struct obj *,long)) SEC20;
E char *FDECL(xprname, (struct obj *,const char *,CHAR_P,BOOLEAN_P,long)) SEC20;
E int NDECL(ddoinv) SEC20;
E char FDECL(display_inventory, (const char *,BOOLEAN_P)) SEC20;
E int FDECL(display_binventory, (int,int,BOOLEAN_P)) SEC20;
E struct obj *FDECL(display_cinventory,(struct obj *)) SEC20;
E struct obj *FDECL(display_minventory,(struct monst *,int)) SEC20;
E int NDECL(dotypeinv) SEC20;
E const char *FDECL(dfeature_at, (int,int,char *)) SEC20;
E int FDECL(look_here, (int,BOOLEAN_P)) SEC20;
E int NDECL(dolook) SEC20;
E void FDECL(feel_cockatrice, (struct obj *,BOOLEAN_P)) SEC20;
E void FDECL(stackobj, (struct obj *)) SEC20;
E int NDECL(doprgold) SEC20;
E int NDECL(doprwep) SEC20;
E int NDECL(doprarm) SEC20;
E int NDECL(doprring) SEC20;
E int NDECL(dopramulet) SEC20;
E int NDECL(doprtool) SEC20;
E int NDECL(doprinuse) SEC20;
E void FDECL(useupf, (struct obj *,long)) SEC20;
E char *FDECL(let_to_name, (CHAR_P,BOOLEAN_P)) SEC20;
E void NDECL(free_invbuf) SEC20;
E void NDECL(reassign) SEC20;
E int NDECL(doorganize) SEC20;
E int FDECL(count_unpaid, (struct obj *)) SEC20;
E void FDECL(carry_obj_effects, (struct obj *)) SEC20;

/* ### ioctl.c ### */

#if defined(UNIX) || defined(__BEOS__)
E void NDECL(getwindowsz);
E void NDECL(getioctls);
E void NDECL(setioctls);
# ifdef SUSPEND
E int NDECL(dosuspend);
# endif /* SUSPEND */
#endif /* UNIX || __BEOS__ */

/* ### light.c ### */

E void FDECL(new_light_source, (XCHAR_P, XCHAR_P, int, int, genericptr_t)) SEC21;
E void FDECL(del_light_source, (int, genericptr_t)) SEC21;
E void FDECL(do_light_sources, (char **)) SEC21;
E struct monst *FDECL(find_mid, (unsigned, unsigned)) SEC21;
E void FDECL(save_light_sources, (int, int, int)) SEC21;
E void FDECL(restore_light_sources, (int)) SEC21;
E void FDECL(relink_light_sources, (BOOLEAN_P)) SEC21;
E void FDECL(obj_move_light_source, (struct obj *, struct obj *)) SEC21;
E boolean NDECL(any_light_source) SEC21;
E void FDECL(snuff_light_source, (int, int)) SEC21;
E boolean FDECL(obj_sheds_light, (struct obj *)) SEC21;
E boolean FDECL(obj_is_burning, (struct obj *)) SEC21;
E void FDECL(obj_split_light_source, (struct obj *, struct obj *)) SEC21;
E void FDECL(obj_merge_light_sources, (struct obj *,struct obj *)) SEC21;
E int FDECL(candle_light_range, (struct obj *)) SEC21;
#ifdef WIZARD
E int NDECL(wiz_light_sources) SEC21;
#endif

/* ### lock.c ### */

#ifdef USE_TRAMPOLI
E int NDECL(forcelock) SEC21;
E int NDECL(picklock) SEC21;
#endif
E boolean FDECL(picking_lock, (int *,int *)) SEC21;
E boolean FDECL(picking_at, (int,int)) SEC21;
E void NDECL(reset_pick) SEC21;
E int FDECL(pick_lock, (struct obj *)) SEC21;
E int NDECL(doforce) SEC21;
E boolean FDECL(boxlock, (struct obj *,struct obj *)) SEC21;
E boolean FDECL(doorlock, (struct obj *,int,int)) SEC21;
E int NDECL(doopen) SEC21;
E int NDECL(doclose) SEC21;

#ifdef MAC
/* These declarations are here because the main code calls them. */

/* ### macfile.c ### */

E int FDECL(maccreat, (const char *,long));
E int FDECL(macopen, (const char *,int,long));
E int FDECL(macclose, (int));
E int FDECL(macread, (int,void *,unsigned));
E int FDECL(macwrite, (int,void *,unsigned));
E long FDECL(macseek, (int,long,short));

/* ### macsnd.c ### */

E void FDECL(mac_speaker, (struct obj *,char *));

/* ### macunix.c ### */

E void FDECL(regularize, (char *));
E void NDECL(getlock);

/* ### macwin.c ### */

E void FDECL(lock_mouse_cursor, (Boolean));
E int NDECL(SanePositions);

/* ### mttymain.c ### */

E void FDECL(getreturn, (char *));
E void VDECL(msmsg, (const char *,...));
E void NDECL(gettty);
E void NDECL(setftty);
E void FDECL(settty, (const char *));
E int NDECL(tgetch);
E void FDECL(cmov, (int x, int y));
E void FDECL(nocmov, (int x, int y));

#endif /* MAC */

/* ### mail.c ### */
//XXX: Is this stuff needed?
#ifdef MAIL
# ifdef UNIX
E void NDECL(getmailstatus) SEC21;
# endif
E void NDECL(ckmailstatus) SEC21;
E void FDECL(readmail, (struct obj *)) SEC21;
#endif /* MAIL */

/* ### makemon.c ### */

E boolean FDECL(is_home_elemental, (struct permonst *)) SEC22;
E struct monst *FDECL(clone_mon, (struct monst *)) SEC22;
E struct monst *FDECL(makemon, (struct permonst *,int,int,int)) SEC22;
E boolean FDECL(create_critters, (int,struct permonst *)) SEC22;
E struct permonst *NDECL(rndmonst) SEC22;
E void FDECL(reset_rndmonst, (int)) SEC22;
E struct permonst *FDECL(mkclass, (CHAR_P,int)) SEC22;
E int FDECL(adj_lev, (struct permonst *)) SEC22;
E struct permonst *FDECL(grow_up, (struct monst *,struct monst *)) SEC22;
E int FDECL(mongets, (struct monst *,int)) SEC22;
E int FDECL(golemhp, (int)) SEC22;
E boolean FDECL(peace_minded, (struct permonst *)) SEC22;
E void FDECL(set_malign, (struct monst *)) SEC22;
E void FDECL(set_mimic_sym, (struct monst *)) SEC22;

/* ### mcastu.c ### */

E int FDECL(castmu, (struct monst *,struct attack *)) SEC22;
E int FDECL(buzzmu, (struct monst *,struct attack *)) SEC22;

/* ### mhitm.c ### */

E int FDECL(fightm, (struct monst *)) SEC23;
E int FDECL(mattackm, (struct monst *,struct monst *)) SEC23;
E int FDECL(noattacks, (struct permonst *)) SEC23;
E int FDECL(sleep_monst, (struct monst *,int,int)) SEC23;
E void FDECL(slept_monst, (struct monst *)) SEC23;

/* ### mhitu.c ### */

E const char *FDECL(mpoisons_subj, (struct monst *,struct attack *)) SEC24;
E void NDECL(u_slow_down) SEC24;
E struct monst *NDECL(cloneu) SEC24;
E void FDECL(expels, (struct monst *,struct permonst *,BOOLEAN_P)) SEC24;
E int FDECL(mattacku, (struct monst *)) SEC25;
E int FDECL(gazemu, (struct monst *,struct attack *)) SEC24;
E void FDECL(mdamageu, (struct monst *,int)) SEC24;
E int FDECL(could_seduce, (struct monst *,struct monst *,struct attack *)) SEC24;
#ifdef SEDUCE
E int FDECL(doseduce, (struct monst *)) SEC24;
#endif

/* ### minion.c ### */

E void FDECL(msummon, (struct permonst *)) SEC25;
E void FDECL(summon_minion, (ALIGNTYP_P,BOOLEAN_P)) SEC25;
E int FDECL(demon_talk, (struct monst *)) SEC25;
E long FDECL(bribe, (struct monst *)) SEC25;
E int FDECL(dprince, (ALIGNTYP_P)) SEC25;
E int FDECL(dlord, (ALIGNTYP_P)) SEC25;
E int NDECL(llord) SEC25;
E int FDECL(ndemon, (ALIGNTYP_P)) SEC25;
E int NDECL(lminion) SEC25;

/* ### mklev.c ### */

#ifdef USE_TRAMPOLI
E int FDECL(do_comp, (genericptr_t,genericptr_t)) SEC26;
#endif
E void NDECL(sort_rooms) SEC26;
E void FDECL(add_room, (int,int,int,int,BOOLEAN_P,SCHAR_P,BOOLEAN_P)) SEC26;
E void FDECL(add_subroom, (struct mkroom *,int,int,int,int,
			   BOOLEAN_P,SCHAR_P,BOOLEAN_P)) SEC26;
E void NDECL(makecorridors) SEC26;
E void FDECL(add_door, (int,int,struct mkroom *)) SEC26;
E void NDECL(mklev) SEC26;
#ifdef SPECIALIZATION
E void FDECL(topologize, (struct mkroom *,BOOLEAN_P)) SEC26;
#else
E void FDECL(topologize, (struct mkroom *)) SEC26;
#endif
E void FDECL(place_branch, (branch *,XCHAR_P,XCHAR_P)) SEC26;
E boolean FDECL(occupied, (XCHAR_P,XCHAR_P)) SEC26;
E int FDECL(okdoor, (XCHAR_P,XCHAR_P)) SEC26;
E void FDECL(dodoor, (int,int,struct mkroom *)) SEC26;
E void FDECL(mktrap, (int,int,struct mkroom *,coord*)) SEC26;
E void FDECL(mkstairs, (XCHAR_P,XCHAR_P,CHAR_P,struct mkroom *)) SEC26;
E void NDECL(mkinvokearea) SEC26;

/* ### mkmap.c ### */

void FDECL(flood_fill_rm, (int,int,int,BOOLEAN_P,BOOLEAN_P)) SEC26;

/* ### mkmaze.c ### */

E void FDECL(wallification, (int,int,int,int)) SEC27;
E void FDECL(walkfrom, (int,int)) SEC27;
E void FDECL(makemaz, (const char *)) SEC27;
E void FDECL(mazexy, (coord *)) SEC27;
E void NDECL(bound_digging) SEC27;
E void FDECL(mkportal, (XCHAR_P,XCHAR_P,XCHAR_P,XCHAR_P)) SEC27;
E boolean FDECL(bad_location, (XCHAR_P,XCHAR_P,XCHAR_P,XCHAR_P,XCHAR_P,XCHAR_P)) SEC27;
E void FDECL(place_lregion, (XCHAR_P,XCHAR_P,XCHAR_P,XCHAR_P,
			     XCHAR_P,XCHAR_P,XCHAR_P,XCHAR_P,
			     XCHAR_P,d_level *)) SEC27;
E void NDECL(movebubbles) SEC27;
E void NDECL(water_friction) SEC27;
E void FDECL(save_waterlevel, (int,int)) SEC27;
E void FDECL(restore_waterlevel, (int)) SEC27;

/* ### mkobj.c ### */

E struct obj *FDECL(mkobj_at, (CHAR_P,int,int,BOOLEAN_P)) SEC27;
E struct obj *FDECL(mksobj_at, (int,int,int,BOOLEAN_P)) SEC27;
E struct obj *FDECL(mkobj, (CHAR_P,BOOLEAN_P)) SEC27;
E int NDECL(rndmonnum) SEC27;
E struct obj *FDECL(splitobj, (struct obj *,long)) SEC27;
E void FDECL(replace_object, (struct obj *,struct obj *)) SEC27;
E void FDECL(bill_dummy_object, (struct obj *)) SEC27;
E struct obj *FDECL(mksobj, (int,BOOLEAN_P,BOOLEAN_P)) SEC27;
E int FDECL(bcsign, (struct obj *)) SEC27;
E int FDECL(weight, (struct obj *)) SEC27;
E struct obj *FDECL(mkgold, (long,int,int)) SEC27;
E struct obj *FDECL(mkcorpstat,
		(int,struct monst *,struct permonst *,int,int,BOOLEAN_P)) SEC27;
E struct obj *FDECL(obj_attach_mid, (struct obj *, unsigned)) SEC27;
E struct monst *FDECL(get_mtraits, (struct obj *, BOOLEAN_P)) SEC27;
E struct obj *FDECL(mk_tt_object, (int,int,int)) SEC27;
E struct obj *FDECL(mk_named_object,
			(int,struct permonst *,int,int,const char *)) SEC27;
E struct obj *FDECL(rnd_treefruit_at, (int, int)) SEC27;
E void FDECL(start_corpse_timeout, (struct obj *)) SEC27;
E void FDECL(bless, (struct obj *)) SEC27;
E void FDECL(unbless, (struct obj *)) SEC27;
E void FDECL(curse, (struct obj *)) SEC27;
E void FDECL(uncurse, (struct obj *)) SEC27;
E void FDECL(blessorcurse, (struct obj *,int)) SEC27;
E boolean FDECL(is_flammable, (struct obj *)) SEC27;
E void FDECL(place_object, (struct obj *,int,int)) SEC27;
E void FDECL(remove_object, (struct obj *)) SEC27;
E void FDECL(discard_minvent, (struct monst *)) SEC27;
E void FDECL(obj_extract_self, (struct obj *)) SEC27;
E void FDECL(extract_nobj, (struct obj *, struct obj **)) SEC27;
E void FDECL(extract_nexthere, (struct obj *, struct obj **)) SEC27;
E int FDECL(add_to_minv, (struct monst *, struct obj *)) SEC27;
E void FDECL(add_to_container, (struct obj *, struct obj *)) SEC27;
E void FDECL(add_to_migration, (struct obj *)) SEC27;
E void FDECL(add_to_buried, (struct obj *)) SEC27;
E void FDECL(dealloc_obj, (struct obj *)) SEC27;
E void FDECL(obj_ice_effects, (int, int, BOOLEAN_P)) SEC27;
E long FDECL(peek_at_iced_corpse_age, (struct obj *)) SEC27;
#ifdef WIZARD
E void NDECL(obj_sanity_check) SEC27;
#endif

/* ### mkroom.c ### */

E void FDECL(mkroom, (int)) SEC28;
E void FDECL(fill_zoo, (struct mkroom *)) SEC28;
E boolean FDECL(nexttodoor, (int,int)) SEC28;
E boolean FDECL(has_dnstairs, (struct mkroom *)) SEC28;
E boolean FDECL(has_upstairs, (struct mkroom *)) SEC28;
E int FDECL(somex, (struct mkroom *)) SEC28;
E int FDECL(somey, (struct mkroom *)) SEC28;
E boolean FDECL(inside_room, (struct mkroom *,XCHAR_P,XCHAR_P)) SEC28;
E boolean FDECL(somexy, (struct mkroom *,coord *)) SEC28;
E void FDECL(mkundead, (coord *,BOOLEAN_P,int)) SEC28;
E struct permonst *NDECL(courtmon) SEC28;
E void FDECL(save_rooms, (int)) SEC28;
E void FDECL(rest_rooms, (int)) SEC28;
E struct mkroom *FDECL(search_special, (SCHAR_P)) SEC28;

/* ### mon.c ### */

E int FDECL(undead_to_corpse, (int)) SEC29;
E int FDECL(pm_to_cham, (int)) SEC29;
E int FDECL(minwater, (struct monst *)) SEC29;
E int NDECL(movemon) SEC29;
E int FDECL(meatgold, (struct monst *)) SEC29;
E int FDECL(meatobj, (struct monst *)) SEC29;
E void FDECL(mpickgold, (struct monst *)) SEC29;
E boolean FDECL(mpickstuff, (struct monst *,const char *)) SEC29;
E int FDECL(curr_mon_load, (struct monst *)) SEC29;
E int FDECL(max_mon_load, (struct monst *)) SEC29;
E boolean FDECL(can_carry, (struct monst *,struct obj *)) SEC29;
E int FDECL(mfndpos, (struct monst *,coord *,long *,long)) SEC29;
E boolean FDECL(monnear, (struct monst *,int,int)) SEC29;
E void NDECL(dmonsfree) SEC29;
E int FDECL(mcalcmove, (struct monst*)) SEC29;
E void NDECL(mcalcdistress) SEC29;
E void FDECL(replmon, (struct monst *,struct monst *)) SEC29;
E void FDECL(relmon, (struct monst *)) SEC29;
E struct obj *FDECL(mlifesaver, (struct monst *)) SEC29;
E void FDECL(mondead, (struct monst *)) SEC29;
E void FDECL(mondied, (struct monst *)) SEC29;
E void FDECL(mongone, (struct monst *)) SEC29;
E void FDECL(monstone, (struct monst *)) SEC29;
E void FDECL(monkilled, (struct monst *,const char *,int)) SEC29;
E void FDECL(unstuck, (struct monst *)) SEC29;
E void FDECL(killed, (struct monst *)) SEC29;
E void FDECL(xkilled, (struct monst *,int)) SEC29;
E void FDECL(mon_to_stone, (struct monst*)) SEC29;
E void FDECL(mnexto, (struct monst *)) SEC29;
E boolean FDECL(mnearto, (struct monst *,XCHAR_P,XCHAR_P,BOOLEAN_P)) SEC29;
E void FDECL(poisontell, (int)) SEC29;
E void FDECL(poisoned, (const char *,int,const char *,int)) SEC29;
E void FDECL(m_respond, (struct monst *)) SEC29;
E void FDECL(setmangry, (struct monst *)) SEC29;
E void FDECL(wakeup, (struct monst *)) SEC29;
E void NDECL(wake_nearby) SEC29;
E void FDECL(wake_nearto, (int,int,int)) SEC29;
E void FDECL(seemimic, (struct monst *)) SEC29;
E void NDECL(rescham) SEC29;
E void NDECL(restartcham) SEC29;
E void FDECL(restore_cham, (struct monst *)) SEC29;
E void FDECL(mon_animal_list, (BOOLEAN_P)) SEC29;
E int FDECL(newcham, (struct monst *,struct permonst *)) SEC29;
E int FDECL(can_be_hatched, (int)) SEC29;
E int FDECL(egg_type_from_parent, (int,BOOLEAN_P)) SEC29;
E boolean FDECL(dead_species, (int,BOOLEAN_P)) SEC29;
E void NDECL(kill_genocided_monsters) SEC29;
E void FDECL(golemeffects, (struct monst *,int,int)) SEC29;
E boolean FDECL(angry_guards, (BOOLEAN_P)) SEC29;
E void NDECL(pacify_guards) SEC29;

/* ### mondata.c ### */

E void FDECL(set_mon_data, (struct monst *,struct permonst *,int)) SEC30;
E boolean FDECL(attacktype, (struct permonst *,int)) SEC30;
E boolean FDECL(poly_when_stoned, (struct permonst *)) SEC30;
E boolean FDECL(resists_drli, (struct monst *)) SEC30;
E boolean FDECL(resists_magm, (struct monst *)) SEC30;
E boolean FDECL(resists_blnd, (struct monst *)) SEC30;
E boolean FDECL(can_blnd, (struct monst *,struct monst *,UCHAR_P,struct obj *)) SEC30;
E boolean FDECL(ranged_attk, (struct permonst *)) SEC30;
E boolean FDECL(hates_silver, (struct permonst *)) SEC30;
E boolean FDECL(can_track, (struct permonst *)) SEC30;
E boolean FDECL(breakarm, (struct permonst *)) SEC30;
E boolean FDECL(sliparm, (struct permonst *)) SEC30;
E boolean FDECL(sticks, (struct permonst *)) SEC30;
/* E boolean FDECL(canseemon, (struct monst *)); */
E boolean FDECL(dmgtype, (struct permonst *,int)) SEC30;
E int FDECL(max_passive_dmg, (struct monst *,struct monst *)) SEC30;
E int FDECL(monsndx, (struct permonst *)) SEC30;
E int FDECL(name_to_mon, (const char *)) SEC30;
E int FDECL(gender, (struct monst *)) SEC30;
E int FDECL(pronoun_gender, (struct monst *)) SEC30;
E boolean FDECL(levl_follower, (struct monst *)) SEC30;
E int FDECL(little_to_big, (int)) SEC30;
E int FDECL(big_to_little, (int)) SEC30;
E const char *FDECL(locomotion, (const struct permonst *,const char *)) SEC30;

/* ### monmove.c ### */

E boolean FDECL(itsstuck, (struct monst *)) SEC30;
E boolean FDECL(mb_trapped, (struct monst *)) SEC30;
E void FDECL(mon_regen, (struct monst *,BOOLEAN_P)) SEC30;
E int FDECL(dochugw, (struct monst *)) SEC30;
E boolean FDECL(onscary, (int,int,struct monst *)) SEC30;
E int FDECL(dochug, (struct monst *)) SEC30;
E int FDECL(m_move, (struct monst *,int)) SEC30;
E boolean FDECL(closed_door, (int,int)) SEC30;
E boolean FDECL(accessible, (int,int)) SEC30;
E void FDECL(set_apparxy, (struct monst *)) SEC30;
E boolean FDECL(can_ooze, (struct monst *)) SEC30;

/* ### monst.c ### */

E void NDECL(monst_init) SEC30;

/* ### monstr.c ### */

E void NDECL(monstr_init) SEC30;

/* ### mplayer.c ### */

E struct monst *FDECL(mk_mplayer, (struct permonst *,XCHAR_P,
				   XCHAR_P,BOOLEAN_P)) SEC31;
E void FDECL(create_mplayers, (int,BOOLEAN_P)) SEC31;
E void FDECL(mplayer_talk, (struct monst *)) SEC31;

#ifdef MICRO

/* ### msdos.c,os2.c,tos.c,winnt.c ### */

#  ifndef WIN32
E int NDECL(tgetch);
#  endif
#  ifndef TOS
E char NDECL(switchar);
#  endif
# ifndef __GO32__
E long FDECL(freediskspace, (char *));
#  ifdef MSDOS
E int FDECL(findfirst_file, (char *));
E int NDECL(findnext_file);
E long FDECL(filesize_nh, (char *));
#  else
E int FDECL(findfirst, (char *));
E int NDECL(findnext);
E long FDECL(filesize, (char *));
#  endif /* MSDOS */
E char *NDECL(foundfile_buffer);
# endif /* __GO32__ */
E void FDECL(chdrive, (char *));
# ifndef TOS
E void NDECL(disable_ctrlP);
E void NDECL(enable_ctrlP);
# endif
# if defined(MICRO) && !defined(WINNT)
E void NDECL(get_scr_size);
#  ifndef TOS
E void FDECL(gotoxy, (int,int));
#  endif
# endif
# ifdef TOS
E int FDECL(_copyfile, (char *,char *));
E int NDECL(kbhit);
E void NDECL(set_colors);
E void NDECL(restore_colors);
#  ifdef SUSPEND
E int NDECL(dosuspend);
#  endif
# endif /* TOS */
# ifdef WIN32
E char *FDECL(get_username, (int *));
E void FDECL(nt_regularize, (char *));
E int NDECL((*nt_kbhit));
E void FDECL(Delay, (int));
# endif /* WIN32 */

#endif /* MICRO */

/* ### mthrowu.c ### */

E int FDECL(thitu, (int,int,struct obj *,const char *)) SEC31;
E int FDECL(ohitmon, (struct monst *,struct obj *,int,BOOLEAN_P)) SEC31;
E void FDECL(thrwmu, (struct monst *)) SEC31;
E int FDECL(spitmu, (struct monst *,struct attack *)) SEC31;
E int FDECL(breamu, (struct monst *,struct attack *)) SEC31;
E boolean FDECL(linedup, (XCHAR_P,XCHAR_P,XCHAR_P,XCHAR_P)) SEC31;
E boolean FDECL(lined_up, (struct monst *)) SEC31;
E struct obj *FDECL(m_carrying, (struct monst *,int)) SEC31;
E void FDECL(m_useup, (struct monst *,struct obj *)) SEC31;
E void FDECL(m_throw, (struct monst *,int,int,int,int,int,struct obj *)) SEC31;

/* ### muse.c ### */

E boolean FDECL(find_defensive, (struct monst *)) SEC10;
E int FDECL(use_defensive, (struct monst *)) SEC10;
E int FDECL(rnd_defensive_item, (struct monst *)) SEC10;
E boolean FDECL(find_offensive, (struct monst *)) SEC32;
#ifdef USE_TRAMPOLI
E int FDECL(mbhitm, (struct monst *,struct obj *)) SEC32;
#endif
E int FDECL(use_offensive, (struct monst *)) SEC32;
E int FDECL(rnd_offensive_item, (struct monst *)) SEC32;
E boolean FDECL(find_misc, (struct monst *)) SEC32;
E int FDECL(use_misc, (struct monst *)) SEC32;
E int FDECL(rnd_misc_item, (struct monst *)) SEC32;
E boolean FDECL(searches_for_item, (struct monst *,struct obj *)) SEC32;
E boolean FDECL(mon_reflects, (struct monst *,const char *)) SEC32;
E boolean FDECL(ureflects, (const char *,const char *)) SEC32;
E boolean FDECL(munstone, (struct monst *,BOOLEAN_P)) SEC32;

/* ### music.c ### */

E void NDECL(awaken_soldiers) SEC33;
E int FDECL(do_play_instrument, (struct obj *)) SEC33;

/* ### nhlan.c ### */
#ifdef LAN_FEATURES
E void NDECL(init_lan_features);
E char *NDECL(lan_username);
# ifdef LAN_MAIL
E boolean NDECL(lan_mail_check);
E void FDECL(lan_mail_read, (struct obj *));
E void NDECL(lan_mail_init);
E void NDECL(lan_mail_finish);
E void NDECL(lan_mail_terminate);
# endif
#endif

/* ### nttty.c ### */

#ifdef WIN32CON
E void NDECL(get_scr_size);
E int NDECL(nttty_kbhit);
E void NDECL(nttty_open);
E void NDECL(nttty_rubout);
E int NDECL(tgetch);
E int FDECL(ntposkey,(int *, int *, int *));
#endif

/* ### o_init.c ### */

E void NDECL(init_objects) SEC33;
E int NDECL(find_skates) SEC33;
E void NDECL(oinit) SEC33;
E void FDECL(savenames, (int,int)) SEC33;
E void FDECL(restnames, (int)) SEC33;
E void FDECL(discover_object, (int,BOOLEAN_P,BOOLEAN_P)) SEC33;
E void FDECL(undiscover_object, (int)) SEC33;
E int NDECL(dodiscovered) SEC33;

/* ### objects.c ### */

E void NDECL(objects_init) SEC33;

/* ### objnam.c ### */

E char *FDECL(obj_typename, (int)) SEC34;
E char *FDECL(simple_typename, (int)) SEC34;
E boolean FDECL(obj_is_pname, (struct obj *)) SEC34;
E char *FDECL(distant_name, (struct obj *,char *(*)(OBJ_P))) SEC34;
E char *FDECL(xname, (struct obj *)) SEC34;
E char *FDECL(doname, (struct obj *)) SEC34;
E boolean FDECL(not_fully_identified, (struct obj *)) SEC34;
E const char *FDECL(corpse_xname, (struct obj *,BOOLEAN_P)) SEC34;
E const char *FDECL(singular, (struct obj *,char *(*)(OBJ_P))) SEC34;
E char *FDECL(an, (const char *)) SEC34;
E char *FDECL(An, (const char *)) SEC34;
E char *FDECL(The, (const char *)) SEC34;
E char *FDECL(the, (const char *)) SEC34;
E char *FDECL(aobjnam, (struct obj *,const char *)) SEC34;
E char *FDECL(Doname2, (struct obj *)) SEC34;
E char *FDECL(yname, (struct obj *)) SEC34;
E char *FDECL(Yname2, (struct obj *)) SEC34;
E char *FDECL(makeplural, (const char *)) SEC34;
E char *FDECL(makesingular, (const char *)) SEC34;
E struct obj *FDECL(readobjnam, (char *)) SEC34;
E int FDECL(rnd_class, (int,int)) SEC34;

/* ### options.c ### */

E boolean FDECL(match_optname, (const char *,const char *,int,BOOLEAN_P)) SEC35;
E void NDECL(initoptions) SEC35;
E void FDECL(parseoptions, (char *,BOOLEAN_P,BOOLEAN_P)) SEC35;
E int NDECL(doset) SEC35;
E int NDECL(dotogglepickup) SEC35;
E void NDECL(option_help) SEC35;
E void FDECL(next_opt, (winid,const char *)) SEC35;
E int FDECL(fruitadd, (char *)) SEC35;
E int FDECL(choose_classes_menu, (const char *,int,BOOLEAN_P,char *,char *)) SEC35;
E void FDECL(add_menu_cmd_alias, (CHAR_P, CHAR_P)) SEC35;
E char FDECL(map_menu_cmd, (CHAR_P)) SEC35;
E void FDECL(assign_warnings, (uchar *)) SEC35;
E char *FDECL(nh_getenv, (const char *)) SEC35;

/* ### pager.c ### */

E int NDECL(dowhatis) SEC36;
E int NDECL(doquickwhatis) SEC36;
E int NDECL(doidtrap) SEC36;
E int NDECL(dowhatdoes) SEC36;
E int NDECL(dohelp) SEC36;
E int NDECL(dohistory) SEC36;

/* ### pcmain.c ### */

#if defined(MICRO)
# ifdef CHDIR
E void FDECL(chdirx, (char *,BOOLEAN_P));
# endif /* CHDIR */
#endif /* MICRO */

/* ### pcsys.c ### */

#ifdef MICRO
E void NDECL(flushout);
E int NDECL(dosh);
# ifdef MFLOPPY
E void FDECL(eraseall, (const char *,const char *));
E void FDECL(copybones, (int));
E void NDECL(playwoRAMdisk);
E int FDECL(saveDiskPrompt, (int));
E void NDECL(gameDiskPrompt);
# endif
E void FDECL(append_slash, (char *));
E void FDECL(getreturn, (const char *));
# ifndef AMIGA
E void VDECL(msmsg, (const char *,...));
# endif
E FILE *FDECL(fopenp, (const char *,const char *));
#endif /* MICRO */

/* ### pctty.c ### */

#if defined(MICRO)
E void NDECL(gettty);
E void FDECL(settty, (const char *));
E void NDECL(setftty);
E void VDECL(error, (const char *,...));
#if defined(TIMED_DELAY) && defined(_MSC_VER)
E void FDECL(msleep, (unsigned));
#endif
#endif /* MICRO */

/* ### pcunix.c ### */

#if defined(MICRO)
E void FDECL(gethdate, (char *));
E void FDECL(regularize, (char *));
#endif /* MICRO */
#if defined(PC_LOCKING)
E void NDECL(getlock);
#endif

/* ### pickup.c ### */

E int FDECL(collect_obj_classes,
	(char *,struct obj *,BOOLEAN_P,BOOLEAN_P,boolean FDECL((*),(OBJ_P)))) SEC37;
E void FDECL(add_valid_menu_class, (int)) SEC37;
E boolean FDECL(allow_all, (struct obj *)) SEC37;
E boolean FDECL(allow_category, (struct obj *)) SEC37;
E boolean FDECL(is_worn_by_type, (struct obj *)) SEC37;
#ifdef USE_TRAMPOLI
E int FDECL(ck_bag, (struct obj *)) SEC37;
E int FDECL(in_container, (struct obj *)) SEC37;
E int FDECL(out_container, (struct obj *)) SEC37;
#endif
E int FDECL(pickup, (int)) SEC37;
E int FDECL(pickup_object, (struct obj *, long, BOOLEAN_P)) SEC37;
E int FDECL(query_category, (const char *, struct obj *, int,
				menu_item **, int)) SEC37;
E int FDECL(query_objlist, (const char *, struct obj *, int,
				menu_item **, int, boolean (*)(OBJ_P))) SEC37;
E struct obj *FDECL(pick_obj, (struct obj *)) SEC37;
E int NDECL(encumber_msg) SEC37;
E int NDECL(doloot) SEC37;
E int FDECL(use_container, (struct obj *,int)) SEC37;

/* ### pline.c ### */

E void VDECL(pline, (const char *,...)) PRINTF_F(1,2) SEC37;
E void VDECL(Norep, (const char *,...)) PRINTF_F(1,2) SEC37;
E void NDECL(free_youbuf) SEC37;
E void VDECL(You, (const char *,...)) PRINTF_F(1,2) SEC37;
E void VDECL(Your, (const char *,...)) PRINTF_F(1,2) SEC37;
E void VDECL(You_feel, (const char *,...)) PRINTF_F(1,2) SEC37;
E void VDECL(You_cant, (const char *,...)) PRINTF_F(1,2) SEC37;
E void VDECL(You_hear, (const char *,...)) PRINTF_F(1,2) SEC37;
E void VDECL(pline_The, (const char *,...)) PRINTF_F(1,2) SEC37;
E void VDECL(There, (const char *,...)) PRINTF_F(1,2) SEC37;
E void VDECL(verbalize, (const char *,...)) PRINTF_F(1,2) SEC37;
E void VDECL(raw_printf, (const char *,...)) PRINTF_F(1,2) SEC37;
E void VDECL(impossible, (const char *,...)) PRINTF_F(1,2) SEC37;
E const char *FDECL(align_str, (ALIGNTYP_P)) SEC37;
E void FDECL(mstatusline, (struct monst *)) SEC37;
E void NDECL(ustatusline) SEC37;
E void NDECL(self_invis_message) SEC37;

/* ### polyself.c ### */

E void NDECL(set_uasmon) SEC38;
E void NDECL(change_sex) SEC38;
E void NDECL(polyself) SEC38;
E int FDECL(polymon, (int)) SEC38;
E void NDECL(rehumanize) SEC38;
E int NDECL(dobreathe) SEC38;
E int NDECL(dospit) SEC38;
E int NDECL(doremove) SEC38;
E int NDECL(dospinweb) SEC38;
E int NDECL(dosummon) SEC38;
E int NDECL(doconfuse) SEC38;
E int NDECL(dohide) SEC38;
E int NDECL(domindblast) SEC38;
E const char *FDECL(mbodypart, (struct monst *,int)) SEC38;
E const char *FDECL(body_part, (int)) SEC38;
E int NDECL(poly_gender) SEC38;
E void FDECL(ugolemeffects, (int,int)) SEC38;

/* ### potion.c ### */

E void FDECL(set_itimeout, (long *,long)) SEC39;
E void FDECL(incr_itimeout, (long *,int)) SEC39;
E void FDECL(make_confused, (long,BOOLEAN_P)) SEC39;
E void FDECL(make_stunned, (long,BOOLEAN_P)) SEC39;
E void FDECL(make_blinded, (long,BOOLEAN_P)) SEC39;
E void FDECL(make_sick, (long, const char *, BOOLEAN_P,int)) SEC39;
E void FDECL(make_vomiting, (long,BOOLEAN_P)) SEC39;
E void FDECL(make_hallucinated, (long,BOOLEAN_P,long)) SEC39;
E int NDECL(dodrink) SEC39;
E int FDECL(dopotion, (struct obj *)) SEC39;
E int FDECL(peffects, (struct obj *)) SEC39;
E void FDECL(healup, (int,int,BOOLEAN_P,BOOLEAN_P)) SEC39;
E void FDECL(strange_feeling, (struct obj *,const char *)) SEC39;
E void FDECL(potionhit, (struct monst *,struct obj *,BOOLEAN_P)) SEC39;
E void FDECL(potionbreathe, (struct obj *)) SEC39;
E boolean FDECL(get_wet, (struct obj *)) SEC39;
E int NDECL(dodip) SEC39;
E void FDECL(djinni_from_bottle, (struct obj *)) SEC39;
E struct monst *FDECL(split_mon, (struct monst *,struct monst *)) SEC39;

/* ### pray.c ### */

#ifdef USE_TRAMPOLI
E int NDECL(prayer_done) SEC40;
#endif
E int NDECL(dosacrifice) SEC40;
E boolean FDECL(can_pray, (BOOLEAN_P)) SEC40;
E int NDECL(dopray) SEC40;
E const char *NDECL(u_gname) SEC40;
E int NDECL(doturn) SEC40;
E const char *NDECL(a_gname) SEC40;
E const char *FDECL(a_gname_at, (XCHAR_P x,XCHAR_P y)) SEC40;
E const char *FDECL(align_gname, (ALIGNTYP_P)) SEC40;
E const char *FDECL(halu_gname, (ALIGNTYP_P)) SEC40;
E const char *FDECL(align_gtitle, (ALIGNTYP_P)) SEC40;
E void FDECL(altar_wrath, (int,int)) SEC40;


/* ### priest.c ### */

E int FDECL(move_special, (struct monst *,BOOLEAN_P,SCHAR_P,BOOLEAN_P,BOOLEAN_P,
			   XCHAR_P,XCHAR_P,XCHAR_P,XCHAR_P)) SEC41;
E char FDECL(temple_occupied, (char *)) SEC41;
E int FDECL(pri_move, (struct monst *)) SEC41;
E void FDECL(priestini, (d_level *,struct mkroom *,int,int,BOOLEAN_P)) SEC41;
E char *FDECL(priestname, (struct monst *,char *)) SEC41;
E boolean FDECL(p_coaligned, (struct monst *)) SEC41;
E struct monst *FDECL(findpriest, (CHAR_P)) SEC41;
E void FDECL(intemple, (int)) SEC41;
E void FDECL(priest_talk, (struct monst *)) SEC41;
E struct monst *FDECL(mk_roamer, (struct permonst *,ALIGNTYP_P,
				  XCHAR_P,XCHAR_P,BOOLEAN_P)) SEC41;
E void FDECL(reset_hostility, (struct monst *)) SEC41;
E boolean FDECL(in_your_sanctuary, (struct monst *,XCHAR_P,XCHAR_P)) SEC41;
E void FDECL(ghod_hitsu, (struct monst *)) SEC41;
E void NDECL(angry_priest) SEC41;
E void NDECL(clearpriests) SEC41;
E void FDECL(restpriest, (struct monst *,BOOLEAN_P)) SEC41;

/* ### quest.c ### */

E void NDECL(onquest) SEC41;
E void NDECL(nemdead) SEC41;
E void NDECL(artitouch) SEC41;
E boolean NDECL(ok_to_quest) SEC41;
E void FDECL(leader_speaks, (struct monst *)) SEC41;
E void NDECL(nemesis_speaks) SEC41;
E void FDECL(quest_chat, (struct monst *)) SEC41;
E void FDECL(quest_talk, (struct monst *)) SEC41;
E void FDECL(quest_stat_check, (struct monst *)) SEC41;
E void FDECL(finish_quest, (struct obj *)) SEC41;

/* ### questpgr.c ### */

E void NDECL(load_qtlist) SEC41;
E void NDECL(unload_qtlist) SEC41;
E short FDECL(quest_info, (int)) SEC41;
E const char *NDECL(ldrname) SEC41;
E boolean FDECL(is_quest_artifact, (struct obj*)) SEC41;
E boolean NDECL(leaderless) SEC41;
E void FDECL(com_pager, (int)) SEC41;
E void FDECL(qt_pager, (int)) SEC41;
E struct permonst *NDECL(qt_montype) SEC41;

/* ### random.c ### */

#if defined(RANDOM) && !defined(__GO32__) /* djgpp has its own random */
E void FDECL(srandom, (unsigned));
E char *FDECL(initstate, (unsigned,char *,int));
E char *FDECL(setstate, (char *));
E long NDECL(random);
#endif /* RANDOM */

/* ### read.c ### */

E int NDECL(doread) SEC42;
E boolean FDECL(is_chargeable, (struct obj *)) SEC42;
E void FDECL(recharge, (struct obj *,int)) SEC42;
E void FDECL(forget_objects, (int)) SEC42;
E void FDECL(forget_levels, (int)) SEC42;
E void NDECL(forget_traps) SEC42;
E void FDECL(forget_map, (int)) SEC42;
E int FDECL(seffects, (struct obj *)) SEC42;
#ifdef USE_TRAMPOLI
E void FDECL(set_lit, (int,int,genericptr_t)) SEC42;
#endif
E void FDECL(litroom, (BOOLEAN_P,struct obj *)) SEC42;
E void FDECL(do_genocide, (int)) SEC42;
E void FDECL(punish, (struct obj *)) SEC42;
E void NDECL(unpunish) SEC42;
E boolean FDECL(cant_create, (int *, BOOLEAN_P)) SEC42;
#ifdef WIZARD
E boolean NDECL(create_particular) SEC42;
#endif

/* ### rect.c ### */

E void NDECL(init_rect) SEC42;
E NhRect *FDECL(get_rect, (NhRect *)) SEC42;
E NhRect *NDECL(rnd_rect) SEC42;
E void FDECL(remove_rect, (NhRect *)) SEC42;
E void FDECL(add_rect, (NhRect *)) SEC42;
E void FDECL(split_rects, (NhRect *,NhRect *)) SEC42;

/* ## region.c ### */
E void NDECL(clear_regions) SEC43;
E void NDECL(run_regions) SEC43;
E boolean FDECL(in_out_region, (XCHAR_P,XCHAR_P)) SEC43;
E boolean FDECL(m_in_out_region, (struct monst *,XCHAR_P,XCHAR_P)) SEC43;
E void NDECL(update_player_regions) SEC43;
E void FDECL(update_monster_region, (struct monst *)) SEC43;
E NhRegion *FDECL(visible_region_at, (XCHAR_P,XCHAR_P)) SEC43;
E void FDECL(show_region, (NhRegion*, XCHAR_P, XCHAR_P)) SEC43;
E void FDECL(save_regions, (int,int)) SEC43;
E void FDECL(rest_regions, (int)) SEC43;
E NhRegion* FDECL(create_gas_cloud, (XCHAR_P, XCHAR_P, int, int)) SEC43;

/* ### restore.c ### */

E void FDECL(inven_inuse, (BOOLEAN_P)) SEC43;
E int FDECL(dorecover, (int)) SEC43;
E void NDECL(trickery) SEC43;
E void FDECL(getlev, (int,int,XCHAR_P,BOOLEAN_P)) SEC43;
E void NDECL(minit) SEC43;
E boolean FDECL(lookup_id_mapping, (unsigned, unsigned *)) SEC43;
#ifdef ZEROCOMP
E int FDECL(mread, (int,genericptr_t,unsigned int)) SEC43;
#else
E void FDECL(mread, (int,genericptr_t,unsigned int)) SEC43;
#endif

/* ### rip.c ### */

E void FDECL(genl_outrip, (winid,int)) SEC43;

/* ### rnd.c ### */

E int FDECL(rn2, (int)) SEC43;
E int FDECL(rnl, (int)) SEC43;
E int FDECL(rnd, (int)) SEC43;
E int FDECL(d, (int,int)) SEC43;
E int FDECL(rne, (int)) SEC43;
E int FDECL(rnz, (int)) SEC43;

/* ### role.c ### */

E boolean FDECL(validrole, (int)) SEC43;
E boolean FDECL(validrace, (int, int)) SEC43;
E boolean FDECL(validgend, (int, int, int)) SEC43;
E boolean FDECL(validalign, (int, int, int)) SEC43;
E int NDECL(randrole) SEC43;
E int FDECL(randrace, (int)) SEC43;
E int FDECL(randgend, (int, int)) SEC43;
E int FDECL(randalign, (int, int)) SEC43;
E int FDECL(str2role, (char *)) SEC43;
E int FDECL(str2race, (char *)) SEC43;
E int FDECL(str2gend, (char *)) SEC43;
E int FDECL(str2align, (char *)) SEC43;
E boolean FDECL(ok_role, (int, int, int, int)) SEC43;
E int FDECL(pick_role, (int, int, int)) SEC43;
E boolean FDECL(ok_race, (int, int, int, int)) SEC43;
E int FDECL(pick_race, (int, int, int)) SEC43;
E boolean FDECL(ok_gend, (int, int, int, int)) SEC43;
E int FDECL(pick_gend, (int, int, int)) SEC43;
E boolean FDECL(ok_align, (int, int, int, int)) SEC43;
E int FDECL(pick_align, (int, int, int)) SEC43;
E void NDECL(role_init) SEC43;
E void NDECL(plnamesuffix) SEC43;
E const char *FDECL(Hello, (struct monst *)) SEC43;
E const char *NDECL(Goodbye) SEC43;

/* ### rumors.c ### */

E char *FDECL(getrumor, (int,char *, BOOLEAN_P)) SEC44;
E void FDECL(outrumor, (int,int)) SEC44;
E void FDECL(outoracle, (BOOLEAN_P, BOOLEAN_P)) SEC44;
E void FDECL(save_oracles, (int,int)) SEC44;
E void FDECL(restore_oracles, (int)) SEC44;
E int FDECL(doconsult, (struct monst *)) SEC44;

/* ### save.c ### */

E int NDECL(dosave) SEC44;
#if defined(UNIX) || defined(VMS) || defined(__EMX__)
E void FDECL(hangup, (int)) SEC44;
#endif
E int NDECL(dosave0) SEC44;
#ifdef INSURANCE
E void NDECL(savestateinlock) SEC44;
#endif
#ifdef MFLOPPY
E boolean FDECL(savelev, (int,XCHAR_P,int)) SEC44;
E boolean FDECL(swapin_file, (int)) SEC44;
E void NDECL(co_false) SEC44;
#else
E void FDECL(savelev, (int,XCHAR_P,int)) SEC44;
#endif
E void FDECL(bufon, (int)) SEC44;
E void FDECL(bufoff, (int)) SEC44;
E void FDECL(bflush, (int)) SEC44;
E void FDECL(bwrite, (int,genericptr_t,unsigned int)) SEC44;
E void FDECL(bclose, (int)) SEC44;
E void FDECL(savefruitchn, (int,int)) SEC44;
E void NDECL(free_dungeons) SEC44;
E void NDECL(freedynamicdata) SEC44;

/* ### shk.c ### */

E char *FDECL(shkname, (struct monst *)) SEC45;
E void FDECL(shkgone, (struct monst *)) SEC45;
E void FDECL(set_residency, (struct monst *,BOOLEAN_P)) SEC45;
E void FDECL(replshk, (struct monst *,struct monst *)) SEC45;
E void FDECL(restshk, (struct monst *,BOOLEAN_P)) SEC45;
E char FDECL(inside_shop, (XCHAR_P,XCHAR_P)) SEC45;
E void FDECL(u_left_shop, (char *,BOOLEAN_P)) SEC45;
E void FDECL(u_entered_shop, (char *)) SEC45;
E boolean FDECL(same_price, (struct obj *,struct obj *)) SEC45;
E void NDECL(shopper_financial_report) SEC45;
E int FDECL(inhishop, (struct monst *)) SEC45;
E struct monst *FDECL(shop_keeper, (CHAR_P)) SEC45;
E boolean FDECL(tended_shop, (struct mkroom *)) SEC45;
E void FDECL(delete_contents, (struct obj *)) SEC45;
E void FDECL(obfree, (struct obj *,struct obj *)) SEC45;
E void FDECL(home_shk, (struct monst *,BOOLEAN_P)) SEC45;
E void FDECL(make_happy_shk, (struct monst *,BOOLEAN_P)) SEC45;
E void FDECL(hot_pursuit, (struct monst *)) SEC45;
E void FDECL(make_angry_shk, (struct monst *,XCHAR_P,XCHAR_P)) SEC45;
E int NDECL(dopay) SEC45;
E boolean FDECL(paybill, (BOOLEAN_P)) SEC45;
E void NDECL(finish_paybill) SEC45;
E struct obj *FDECL(find_oid, (unsigned)) SEC45;
E long FDECL(contained_cost, (struct obj *,struct monst *,long,BOOLEAN_P)) SEC45;
E long FDECL(contained_gold, (struct obj *)) SEC45;
E void FDECL(picked_container, (struct obj *)) SEC45;
E long FDECL(unpaid_cost, (struct obj *)) SEC45;
E void FDECL(addtobill, (struct obj *,BOOLEAN_P,BOOLEAN_P,BOOLEAN_P)) SEC46;
E void FDECL(splitbill, (struct obj *,struct obj *)) SEC46;
E void FDECL(subfrombill, (struct obj *,struct monst *)) SEC46;
E long FDECL(stolen_value, (struct obj *,XCHAR_P,XCHAR_P,BOOLEAN_P,BOOLEAN_P)) SEC46;
E void FDECL(sellobj_state, (BOOLEAN_P)) SEC46;
E void FDECL(sellobj, (struct obj *,XCHAR_P,XCHAR_P)) SEC46;
E int FDECL(doinvbill, (int)) SEC46;
E int FDECL(shkcatch, (struct obj *,XCHAR_P,XCHAR_P)) SEC46;
E void FDECL(add_damage, (XCHAR_P,XCHAR_P,long)) SEC46;
E int FDECL(repair_damage, (struct monst *,struct damage *,BOOLEAN_P)) SEC46;
E int FDECL(shk_move, (struct monst *)) SEC46;
E boolean FDECL(is_fshk, (struct monst *)) SEC46;
E void FDECL(shopdig, (int)) SEC46;
E void FDECL(pay_for_damage, (const char *)) SEC46;
E boolean FDECL(costly_spot, (XCHAR_P,XCHAR_P)) SEC46;
E struct obj *FDECL(shop_object, (XCHAR_P,XCHAR_P)) SEC46;
E void FDECL(price_quote, (struct obj *)) SEC46;
E void FDECL(shk_chat, (struct monst *)) SEC46;
E void FDECL(check_unpaid_usage, (struct obj *,BOOLEAN_P)) SEC46;
E void FDECL(check_unpaid, (struct obj *)) SEC46;
E void FDECL(costly_gold, (XCHAR_P,XCHAR_P,long)) SEC46;
E boolean FDECL(block_door, (XCHAR_P,XCHAR_P)) SEC46;
E boolean FDECL(block_entry, (XCHAR_P,XCHAR_P)) SEC46;
E char *FDECL(shk_your, (char *,struct obj *)) SEC46;
E char *FDECL(Shk_Your, (char *,struct obj *)) SEC46;

/* ### shknam.c ### */

E void FDECL(stock_room, (int,struct mkroom *)) SEC46;
E boolean FDECL(saleable, (struct monst *,struct obj *)) SEC46;
E int FDECL(get_shop_item, (int)) SEC46;

/* ### sit.c ### */

E void NDECL(take_gold) SEC47;
E int NDECL(dosit) SEC47;
E void NDECL(rndcurse) SEC47;
E void NDECL(attrcurse) SEC47;

/* ### sounds.c ### */

E void NDECL(dosounds) SEC47;
E const char *FDECL(growl_sound, (struct monst *)) SEC47;
E void FDECL(growl, (struct monst *)) SEC47;
E void FDECL(yelp, (struct monst *)) SEC47;
E void FDECL(whimper, (struct monst *)) SEC47;
E void FDECL(beg, (struct monst *)) SEC47;
E int NDECL(dotalk) SEC47;


/* ### sys/msdos/sound.c ### */

#ifdef MSDOS
E int FDECL(assign_soundcard, (char *));
#endif

/* ### sp_lev.c ### */

E boolean FDECL(check_room, (xchar *,xchar *,xchar *,xchar *,BOOLEAN_P)) SEC48;
E boolean FDECL(create_room, (XCHAR_P,XCHAR_P,XCHAR_P,XCHAR_P,
			      XCHAR_P,XCHAR_P,XCHAR_P,XCHAR_P)) SEC48;
E void FDECL(create_secret_door, (struct mkroom *,XCHAR_P)) SEC48;
E boolean FDECL(dig_corridor, (coord *,coord *,BOOLEAN_P,SCHAR_P,SCHAR_P)) SEC48;
E void FDECL(fill_room, (struct mkroom *,BOOLEAN_P)) SEC48;
E boolean FDECL(load_special, (const char *)) SEC48;

/* ### spell.c ### */

#ifdef USE_TRAMPOLI
E int NDECL(learn) SEC49;
#endif
E int FDECL(study_book, (struct obj *)) SEC49;
E void FDECL(book_substitution, (struct obj *,struct obj *)) SEC49;
E void NDECL(age_spells) SEC49;
E int NDECL(docast) SEC49;
E int FDECL(spell_skilltype, (int)) SEC49;
E int FDECL(spelleffects, (int,BOOLEAN_P)) SEC49;
E void NDECL(losespells) SEC49;
E int NDECL(dovspell) SEC49;
E void FDECL(initialspell, (struct obj *)) SEC49;

/* ### steal.c ### */

#ifdef USE_TRAMPOLI
E int NDECL(stealarm) SEC49;
#endif
E long NDECL(somegold) SEC49;
E void FDECL(stealgold, (struct monst *)) SEC49;
E void FDECL(remove_worn_item, (struct obj *)) SEC49;
E int FDECL(steal, (struct monst *)) SEC49;
E int FDECL(mpickobj, (struct monst *,struct obj *)) SEC49;
E void FDECL(stealamulet, (struct monst *)) SEC49;
E void FDECL(relobj, (struct monst *,int,BOOLEAN_P)) SEC49;

/* ### steed.c ### */

#ifdef STEED
E boolean FDECL(can_saddle, (struct monst *)) SEC49;
E int FDECL(use_saddle, (struct obj *)) SEC49;
E boolean FDECL(can_ride, (struct monst *)) SEC49;
E int NDECL(doride) SEC49;
E boolean FDECL(mount_steed, (struct monst *, BOOLEAN_P)) SEC49;
E void NDECL(exercise_steed) SEC49;
E void NDECL(kick_steed) SEC49;
E void FDECL(dismount_steed, (int)) SEC49;
E void FDECL(place_monster, (struct monst *,int,int)) SEC49;
#endif

/* ### teleport.c ### */

E boolean FDECL(goodpos, (int,int,struct monst *)) SEC50;
E boolean FDECL(enexto, (coord *,XCHAR_P,XCHAR_P,struct permonst *)) SEC50;
E void FDECL(teleds, (int,int)) SEC50;
E boolean NDECL(safe_teleds) SEC50;
E boolean FDECL(teleport_pet, (struct monst *,BOOLEAN_P)) SEC50;
E void NDECL(tele) SEC50;
E int NDECL(dotele) SEC50;
E void NDECL(level_tele) SEC50;
E void FDECL(domagicportal, (struct trap *)) SEC50;
E void FDECL(tele_trap, (struct trap *)) SEC50;
E void FDECL(level_tele_trap, (struct trap *)) SEC50;
E void FDECL(rloc_to, (struct monst *,int,int)) SEC50;
E void FDECL(rloc, (struct monst *)) SEC50;
E boolean FDECL(tele_restrict, (struct monst *)) SEC50;
E void FDECL(mtele_trap, (struct monst *, struct trap *,int)) SEC50;
E int FDECL(mlevel_tele_trap, (struct monst *, struct trap *,BOOLEAN_P,int)) SEC50;
E void FDECL(rloco, (struct obj *)) SEC50;
E int NDECL(random_teleport_level) SEC50;
E boolean FDECL(u_teleport_mon, (struct monst *,BOOLEAN_P)) SEC50;

/* ### tile.c ### */
#ifdef USE_TILES
E void FDECL(substitute_tiles, (d_level *));
#endif

/* ### timeout.c ### */

E void NDECL(burn_away_slime) SEC50;
E void NDECL(nh_timeout) SEC50;
E void FDECL(fall_asleep, (int, BOOLEAN_P)) SEC50;
E void FDECL(attach_egg_hatch_timeout, (struct obj *)) SEC50;
E void FDECL(attach_fig_transform_timeout, (struct obj *)) SEC50;
E void FDECL(kill_egg, (struct obj *)) SEC50;
E void FDECL(hatch_egg, (genericptr_t, long)) SEC50;
E void FDECL(learn_egg_type, (int)) SEC50;
E void FDECL(burn_object, (genericptr_t, long)) SEC50;
E void FDECL(begin_burn, (struct obj *, BOOLEAN_P)) SEC50;
E void FDECL(end_burn, (struct obj *, BOOLEAN_P)) SEC50;
E void NDECL(do_storms) SEC50;
E boolean FDECL(start_timer, (long, SHORT_P, SHORT_P, genericptr_t)) SEC50;
E long FDECL(stop_timer, (SHORT_P, genericptr_t)) SEC50;
E void NDECL(run_timers) SEC50;
E void FDECL(obj_move_timers, (struct obj *, struct obj *)) SEC50;
E void FDECL(obj_split_timers, (struct obj *, struct obj *)) SEC50;
E void FDECL(obj_stop_timers, (struct obj *)) SEC50;
E boolean FDECL(obj_is_local, (struct obj *)) SEC50;
E void FDECL(save_timers, (int,int,int)) SEC50;
E void FDECL(restore_timers, (int,int,BOOLEAN_P,long)) SEC50;
E void FDECL(relink_timers, (BOOLEAN_P)) SEC50;
#ifdef WIZARD
E int NDECL(wiz_timeout_queue) SEC50;
E void NDECL(timer_sanity_check) SEC50;
#endif

/* ### topten.c ### */

E void FDECL(topten, (int)) SEC51;
E void FDECL(prscore, (int,char **)) SEC51;
E struct obj *FDECL(tt_oname, (struct obj *)) SEC51;

/* ### track.c ### */

E void NDECL(initrack) SEC51;
E void NDECL(settrack) SEC51;
E coord *FDECL(gettrack, (int,int)) SEC51;

/* ### trap.c ### */

E boolean FDECL(burnarmor,(struct monst *)) SEC31;
E boolean FDECL(rust_dmg, (struct obj *,const char *,int,BOOLEAN_P,struct monst *)) SEC31;
E void FDECL(grease_protect, (struct obj *,const char *,BOOLEAN_P,struct monst *)) SEC52;
E struct trap *FDECL(maketrap, (int,int,int)) SEC52;
E void FDECL(fall_through, (BOOLEAN_P)) SEC52;
E struct monst *FDECL(animate_statue, (struct obj *,XCHAR_P,XCHAR_P,int,int *)) SEC52;
E struct monst *FDECL(activate_statue_trap,
			(struct trap *,XCHAR_P,XCHAR_P,BOOLEAN_P)) SEC52;
E void FDECL(dotrap, (struct trap *)) SEC52;
E void FDECL(seetrap, (struct trap *)) SEC52;
E int FDECL(mintrap, (struct monst *)) SEC52;
E void FDECL(instapetrify, (const char *)) SEC52;
E void FDECL(minstapetrify, (struct monst *,BOOLEAN_P)) SEC52;
E void FDECL(selftouch, (const char *)) SEC52;
E void FDECL(mselftouch, (struct monst *,const char *,BOOLEAN_P)) SEC52;
E void NDECL(float_up) SEC52;
E void FDECL(fill_pit, (int,int)) SEC52;
E int FDECL(float_down, (long, long)) SEC52;
E void FDECL(water_damage, (struct obj *,BOOLEAN_P,BOOLEAN_P)) SEC53;
E boolean NDECL(drown) SEC53;
E void FDECL(drain_en, (int)) SEC53;
E int NDECL(dountrap) SEC53;
E int FDECL(untrap, (BOOLEAN_P)) SEC53;
E boolean FDECL(chest_trap, (struct obj *,int,BOOLEAN_P)) SEC53;
E void FDECL(deltrap, (struct trap *)) SEC53;
E boolean FDECL(delfloortrap, (struct trap *)) SEC53;
E struct trap *FDECL(t_at, (int,int)) SEC53;
E void FDECL(b_trapped, (const char *,int)) SEC53;
E boolean NDECL(unconscious) SEC53;
E boolean NDECL(lava_effects) SEC53;
E int FDECL(launch_obj,(SHORT_P,int,int,int,int,int)) SEC52;

/* ### u_init.c ### */

E void NDECL(u_init) SEC53;

/* ### uhitm.c ### */

E void FDECL(hurtmarmor,(struct monst *,int)) SEC54;
E boolean FDECL(attack_checks, (struct monst *,struct obj *)) SEC54;
E schar FDECL(find_roll_to_hit, (struct monst *)) SEC54;
E boolean FDECL(attack, (struct monst *)) SEC60;
E boolean FDECL(hmon, (struct monst *,struct obj *,int)) SEC60;
E int FDECL(damageum, (struct monst *,struct attack *)) SEC54;
E void FDECL(missum, (struct monst *,struct attack *)) SEC60;
E int FDECL(passive, (struct monst *,BOOLEAN_P,int,UCHAR_P)) SEC60;
E void FDECL(stumble_onto_mimic, (struct monst *)) SEC60;
E int FDECL(flash_hits_mon, (struct monst *,struct obj *)) SEC60;

/* ### unixmain.c ### */

#ifdef UNIX
# ifdef PORT_HELP
E void NDECL(port_help);
# endif
#endif /* UNIX */


/* ### unixtty.c ### */

#if defined(UNIX) || defined(__BEOS__)
E void NDECL(gettty);
E void FDECL(settty, (const char *));
E void NDECL(setftty);
E void NDECL(intron);
E void NDECL(introff);
E void VDECL(error, (const char *,...)) PRINTF_F(1,2);
#endif /* UNIX || __BEOS__ */

/* ### unixunix.c ### */

#ifdef UNIX
E void FDECL(gethdate, (const char *));
E void NDECL(getlock);
E void FDECL(regularize, (char *));
# ifdef SHELL
E int NDECL(dosh);
# endif /* SHELL */
# if defined(SHELL) || defined(DEF_PAGER) || defined(DEF_MAILREADER)
E int FDECL(child, (int));
# endif
#endif /* UNIX */

/* ### vault.c ### */

E boolean FDECL(grddead, (struct monst *)) SEC55;
E char FDECL(vault_occupied, (char *)) SEC55;
E void NDECL(invault) SEC55;
E int FDECL(gd_move, (struct monst *)) SEC55;
E void NDECL(paygd) SEC55;
E long NDECL(hidden_gold) SEC55;
E boolean NDECL(gd_sound) SEC55;

/* ### version.c ### */

E char *FDECL(getversionstring, (char *)) SEC55;
E int NDECL(doversion) SEC55;
E int NDECL(doextversion) SEC55;
#ifdef MICRO
E boolean FDECL(comp_times, (long)) SEC55;
#endif
E boolean FDECL(check_version, (struct version_info *,
				const char *,BOOLEAN_P)) SEC55;
E unsigned long FDECL(get_feature_notice_ver, (char *)) SEC55;
E unsigned long NDECL(get_current_feature_ver) SEC55;

/* ### video.c ### */

#ifdef MSDOS
E int FDECL(assign_video, (char *));
# ifdef NO_TERMS
E void NDECL(gr_init);
E void NDECL(gr_finish);
# endif
E void FDECL(tileview,(BOOLEAN_P));
#endif
#ifdef VIDEOSHADES
E int FDECL(assign_videoshades, (char *));
E int FDECL(assign_videocolors, (char *));
#endif

/* ### vis_tab.c ### */

#ifdef VISION_TABLES
E void NDECL(vis_tab_init) SEC55;
#endif

/* ### vision.c ### */

E void NDECL(vision_init) SEC55;
E int FDECL(does_block, (int,int,struct rm*)) SEC55;
E void NDECL(vision_reset) SEC55;
E void FDECL(vision_recalc, (int)) SEC55;
E void FDECL(block_point, (int,int)) SEC55;
E void FDECL(unblock_point, (int,int)) SEC55;
E boolean FDECL(clear_path, (int,int,int,int)) SEC55;
E void FDECL(do_clear_area, (int,int,int,
			     void (*)(int,int,genericptr_t),genericptr_t)) SEC55;

#ifdef VMS

/* ### vmsfiles.c ### */

E int FDECL(vms_link, (const char *,const char *));
E int FDECL(vms_unlink, (const char *));
E int FDECL(vms_creat, (const char *,unsigned int));
E int FDECL(vms_open, (const char *,int,unsigned int));
E boolean FDECL(same_dir, (const char *,const char *));
E int FDECL(c__translate, (int));
E char *FDECL(vms_basename, (const char *));

/* ### vmsmail.c ### */

E unsigned long NDECL(init_broadcast_trapping);
E unsigned long NDECL(enable_broadcast_trapping);
E unsigned long NDECL(disable_broadcast_trapping);
# if 0
E struct mail_info *NDECL(parse_next_broadcast);
# endif /*0*/

/* ### vmsmain.c ### */

E int FDECL(main, (int, char **));
# ifdef CHDIR
E void FDECL(chdirx, (const char *,BOOLEAN_P));
# endif /* CHDIR */

/* ### vmsmisc.c ### */

E void NDECL(vms_abort);
E void FDECL(vms_exit, (int));

/* ### vmstty.c ### */

E int NDECL(vms_getchar);
E void NDECL(gettty);
E void FDECL(settty, (const char *));
E void FDECL(shuttty, (const char *));
E void NDECL(setftty);
E void NDECL(intron);
E void NDECL(introff);
E void VDECL(error, (const char *,...)) PRINTF_F(1,2);
#ifdef TIMED_DELAY
E void FDECL(msleep, (unsigned));
#endif

/* ### vmsunix.c ### */

E void FDECL(gethdate, (const char *));
E void NDECL(getlock);
E void FDECL(regularize, (char *));
E int NDECL(vms_getuid);
E boolean FDECL(file_is_stmlf, (int));
E int FDECL(vms_define, (const char *,const char *,int));
E int FDECL(vms_putenv, (const char *));
E char *NDECL(verify_termcap);
# if defined(CHDIR) || defined(SHELL) || defined(SECURE)
E void NDECL(privoff);
E void NDECL(privon);
# endif
# ifdef SHELL
E int NDECL(dosh);
# endif
# if defined(SHELL) || defined(MAIL)
E int FDECL(vms_doshell, (const char *,BOOLEAN_P));
# endif
# ifdef SUSPEND
E int NDECL(dosuspend);
# endif

#endif /* VMS */

/* ### weapon.c ### */

E int FDECL(hitval, (struct obj *,struct monst *)) SEC56;
E int FDECL(dmgval, (struct obj *,struct monst *)) SEC56;
E struct obj *FDECL(select_rwep, (struct monst *)) SEC56;
E struct obj *FDECL(select_hwep, (struct monst *)) SEC56;
E void FDECL(possibly_unwield, (struct monst *)) SEC56;
E int FDECL(mon_wield_item, (struct monst *)) SEC56;
E int NDECL(abon) SEC56;
E int NDECL(dbon) SEC56;
E int NDECL(enhance_weapon_skill) SEC56;
E void FDECL(unrestrict_weapon_skill, (int)) SEC56;
E void FDECL(use_skill, (int,int)) SEC56;
E void FDECL(add_weapon_skill, (int)) SEC56;
E void FDECL(lose_weapon_skill, (int)) SEC56;
E int FDECL(weapon_type, (struct obj *)) SEC56;
E int NDECL(uwep_skill_type) SEC56;
E int FDECL(weapon_hit_bonus, (struct obj *)) SEC56;
E int FDECL(weapon_dam_bonus, (struct obj *)) SEC56;
E void FDECL(skill_init, (struct def_skill *)) SEC56;

/* ### were.c ### */

E void FDECL(were_change, (struct monst *)) SEC56;
E void FDECL(new_were, (struct monst *)) SEC56;
E boolean FDECL(were_summon, (struct permonst *,BOOLEAN_P)) SEC56;
E void NDECL(you_were) SEC56;
E void FDECL(you_unwere, (BOOLEAN_P)) SEC56;

/* ### wield.c ### */

E void FDECL(setuwep, (struct obj *)) SEC56;
E void FDECL(setuqwep, (struct obj *)) SEC56;
E void FDECL(setuswapwep, (struct obj *)) SEC56;
E int NDECL(dowield) SEC56;
E int NDECL(doswapweapon) SEC56;
E int NDECL(dowieldquiver) SEC56;
E int NDECL(dotwoweapon) SEC56;
E int NDECL(can_twoweapon) SEC56;
E void NDECL(untwoweapon) SEC56;
E void NDECL(uwepgone) SEC56;
E void NDECL(uswapwepgone) SEC56;
E void NDECL(uqwepgone) SEC56;
E void FDECL(erode_weapon, (struct obj *,BOOLEAN_P)) SEC56;
E int FDECL(chwepon, (struct obj *,int)) SEC56;
E int FDECL(welded, (struct obj *)) SEC56;
E void FDECL(weldmsg, (struct obj *)) SEC56;

/* ### windows.c ### */

E void FDECL(choose_windows, (const char *)) SEC56;
E char FDECL(genl_message_menu, (CHAR_P,int,const char *)) SEC56;

/* ### wizard.c ### */

E void NDECL(amulet) SEC56;
E int FDECL(mon_has_amulet, (struct monst *)) SEC56;
E int FDECL(mon_has_special, (struct monst *)) SEC56;
E int FDECL(tactics, (struct monst *)) SEC56;
E void NDECL(aggravate) SEC56;
E void NDECL(clonewiz) SEC56;
E int NDECL(pick_nasty) SEC56;
E void FDECL(nasty, (struct monst*)) SEC56;
E void NDECL(resurrect) SEC56;
E void NDECL(intervene) SEC56;
E void NDECL(wizdead) SEC56;
E void FDECL(cuss, (struct monst *)) SEC56;

/* ### worm.c ### */

E int NDECL(get_wormno) SEC57;
E void FDECL(initworm, (struct monst *,int)) SEC57;
E void FDECL(worm_move, (struct monst *)) SEC57;
E void FDECL(worm_nomove, (struct monst *)) SEC57;
E void FDECL(wormgone, (struct monst *)) SEC57;
E void FDECL(wormhitu, (struct monst *)) SEC57;
E void FDECL(cutworm, (struct monst *,XCHAR_P,XCHAR_P,struct obj *)) SEC57;
E void FDECL(see_wsegs, (struct monst *)) SEC57;
E void FDECL(save_worm, (int,int)) SEC57;
E void FDECL(rest_worm, (int)) SEC57;
E void FDECL(place_wsegs, (struct monst *)) SEC57;
E void FDECL(remove_worm, (struct monst *)) SEC57;
E void FDECL(place_worm_tail_randomly, (struct monst *,XCHAR_P,XCHAR_P)) SEC57;
E int FDECL(count_wsegs, (struct monst *)) SEC57;
E boolean FDECL(worm_known, (struct monst *)) SEC57;

/* ### worn.c ### */

E void FDECL(setworn, (struct obj *,long)) SEC57;
E void FDECL(setnotworn, (struct obj *)) SEC57;
E void FDECL(mon_set_minvis, (struct monst *)) SEC57;
E void FDECL(mon_adjust_speed, (struct monst *,int)) SEC57;
E void FDECL(update_mon_intrinsics, (struct monst *,struct obj *,BOOLEAN_P)) SEC57;
E int FDECL(find_mac, (struct monst *)) SEC57;
E void FDECL(m_dowear, (struct monst *,BOOLEAN_P)) SEC57;
E struct obj *FDECL(which_armor, (struct monst *,long)) SEC57;
E void FDECL(mon_break_armor, (struct monst *)) SEC57;

/* ### write.c ### */

E int FDECL(dowrite, (struct obj *)) SEC57;

/* ### zap.c ### */

E int FDECL(bhitm, (struct monst *,struct obj *)) SEC60;
E void FDECL(probe_monster, (struct monst *)) SEC60;
E boolean FDECL(get_obj_location, (struct obj *,xchar *,xchar *,int)) SEC60;
E boolean FDECL(get_mon_location, (struct monst *,xchar *,xchar *,int)) SEC60;
E struct monst *FDECL(montraits, (struct obj *,coord *)) SEC60;
E struct monst *FDECL(revive, (struct obj *)) SEC60;
E int FDECL(unturn_dead, (struct monst *)) SEC58;
E void FDECL(cancel_item, (struct obj *)) SEC58;
E boolean FDECL(drain_item, (struct obj *)) SEC58;
E void FDECL(poly_obj, (struct obj *, int)) SEC58;
E boolean FDECL(obj_resists, (struct obj *,int,int)) SEC58;
E boolean FDECL(obj_shudders, (struct obj *)) SEC58;
E void FDECL(do_osshock, (struct obj *)) SEC58;
E int FDECL(bhito, (struct obj *,struct obj *)) SEC58;
E int FDECL(bhitpile, (struct obj *,int (*)(OBJ_P,OBJ_P),int,int)) SEC58;
E int FDECL(zappable, (struct obj *)) SEC58;
E void FDECL(zapnodir, (struct obj *)) SEC58;
E int NDECL(dozap) SEC58;
E int FDECL(zapyourself, (struct obj *,BOOLEAN_P)) SEC58;
E void FDECL(cancel_monst, (struct monst *,struct obj *,
			    BOOLEAN_P,BOOLEAN_P,BOOLEAN_P)) SEC58;
E void FDECL(weffects, (struct obj *)) SEC58;
E int NDECL(spell_damage_bonus) SEC59;
E const char *FDECL(exclam, (int force)) SEC59;
E void FDECL(hit, (const char *,struct monst *,const char *)) SEC59;
E void FDECL(miss, (const char *,struct monst *)) SEC59;
E struct monst *FDECL(bhit, (int,int,int,int,int (*)(MONST_P,OBJ_P),
			     int (*)(OBJ_P,OBJ_P),struct obj *)) SEC59;
E struct monst *FDECL(boomhit, (int,int)) SEC59;
E int FDECL(burn_floor_paper, (int,int,BOOLEAN_P)) SEC59;
E void FDECL(buzz, (int,int,XCHAR_P,XCHAR_P,int,int)) SEC58;
E void FDECL(melt_ice, (XCHAR_P,XCHAR_P)) SEC59;
E int FDECL(zap_over_floor, (XCHAR_P,XCHAR_P,int,boolean *)) SEC59;
E void FDECL(fracture_rock, (struct obj *)) SEC59;
E boolean FDECL(break_statue, (struct obj *)) SEC59;
E void FDECL(destroy_item, (int,int)) SEC59;
E int FDECL(destroy_mitem, (struct monst *,int,int)) SEC59;
E int FDECL(resist, (struct monst *,CHAR_P,int,int)) SEC59;
E void NDECL(makewish) SEC59;

#endif /* !MAKEDEFS_C && !LEV_LEX_C */

#undef E

#endif /* EXTERN_H */
