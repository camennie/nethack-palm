/* palmconf.h:  Copyright 2000 by Karl Garrison <kdgarrison@home.com>.  This
file may be distributed under the terms of the Nethack license. */

#ifndef PALMCONF_H
#define PALMCONF_H

/* #include <Core/System/FileStream.h> */ /* try to avoid OS 3+ features */
//#include <Core/Pilot.h>
//#include <Core/Common.h>
#include<PalmOS.h>
#include <Core/System/TimeMgr.h>
#include <Core/System/StringMgr.h>
#include <Core/System/DateTime.h>
#include <Core/System/SysUtils.h>

#define SEC43 __attribute__ ((section("S43")))
#define SEC44 __attribute__ ((section("S44")))

#define NO_SIGNAL

/* File stuff */
/* FIXME */
/*
#define FILE FileHand
#define EOF fileErrEOF
*/
#define FILE void*
//#define EOF 0

#define FCMASK 0    /* No default file creation mask */
#define stdout 1    /* temporary kludge to get around some errors */
#define BUFSIZ 0    /* No unbuffered file I/O functions available in PalmOS */
#define palm_open open      /* Defined in palm.c */
#define palm_close close    /* Defined in palm.c */

#define O_RDONLY   00 /* These are used by palm_creat, palm_open in palm.c */
#define O_WRONLY   01
#define O_CREAT  0100
#define O_TRUNC 01000

/* String stuff */

#define strlen StrLen
#define sprintf	StrPrintF
#define Sprintf	StrPrintF
#define Strcat StrCat
#define Strcpy StrCopy

#define FILENAME 32

/* PalmOS uses DateTimeType instead of the standard tm structure, so we
define tm and time_t here ourselves.  This gets used in palmunix.c. */

/* XXX: Does this need to be done? time_t is declared as DWord in DateTime.h (CM) */
struct tm
{
    UInt16 tm_sec;
    UInt16 tm_min;
    UInt16 tm_hour;
    UInt16 tm_mday;
    UInt16 tm_mon;
    UInt16 tm_year;
    UInt16 tm_wday;
    UInt16 tm_yday;
    UInt16 tm_isdst;
};

/* Not certain how this should be defined here */
#define off_t long

#define time_t Int32

/* FIXME */
void *malloc(unsigned int) SEC43;
void free(void *) SEC43;
int close(int) SEC43;
int strncmp(const char *, const char *, unsigned int) SEC43;
char *strcat(char *, const char *) SEC43;
char *getenv(const char *) SEC43;
char *strncpy(char *, const char *, int) SEC43;
int fread( void *ptr, int size, int nmemb, void *stream) SEC43;
void *fopen (const char *path, const char *mode) SEC43;
int fclose( void *stream) SEC43;
char *strncat(char *dest, const char *src, int n) SEC43;
char *rindex(const char *s, int c) SEC43;
int creat(const char *pathname, int mode) SEC43;
int open(const char *pathname, int flags, int mode) SEC43;
int unlink(const char *pathname) SEC43;
int rename(const char *oldpath, const char *newpath) SEC43;
int getuid(void) SEC43;
int write(int fd, const void *buf, int count) SEC43;
void regularize(char *) SEC43;
char *fgets(char *s, int size, void *stream) SEC43;
struct tm *localtime(const time_t *timep) SEC43;
void srand(unsigned int seed) SEC43;
int time(time_t *t) SEC43;
char *strcpy(char *dest, const char *src) SEC43;
char *index(const char *s, int c) SEC43;
int sscanf( const char *str, const char *format, ...) SEC43;
void qsort(void *base, int nmemb, int size,int (*compar)(const void *, const void *)) SEC43;
int atoi(const char *nptr) SEC43;
void error(const char *, ...) SEC43;
int fseek(void *stream, long offset, int whence) SEC43;
int lseek(int fildes, int offset, int whence) SEC43;
int read(int fd, void *buf, int count) SEC43;
int Rand(void) SEC43;
long ftell(void *stream) SEC43;
int fgetc(void *stream) SEC43;
int fscanf(void *stream, const char *format, ...) SEC43;
int fprintf(void *stream, const char *format, ...) SEC43;
void rewind(void *stream) SEC43;
int _cleanup() SEC43;
void _exit() SEC43;
char *getdate() SEC43;
void gethdate(const char *name) SEC43;
void glo(register int foo) SEC43;

#define Free free

#endif  /* PALMCONF_H */
