/* palmunix.c:  Copyright 2000 by Karl Garrison <kdgarrison@home.com>.  This
file may be distributed under the terms of the Nethack license. */

#include "hack.h"
#include "palmconf.h"

//int Printf() { return 0;}
void srand(unsigned int seed) {seed=0;}
int Rand() { return 0;}
//char SAVEF[];
//int Vprintf() { return 0;}
//int Vsprintf() { return 0;}
int _cleanup() { return 0;}	//what is this?
void _exit() {}				//what is this?
//int askname() { return 0;}
int time(time_t *t) {t=NULL; return 0;}
struct tm *localtime(const time_t *timep) {timep=NULL; return NULL;}
int getuid(void) {return 0;}
int open(const char *pathname, int zflags, int mode) {pathname=NULL; zflags=0; mode=0; return 0;}
int close(int fd) {fd=0; return 0;}
int rename(const char *oldpath, const char *newpath) {oldpath=NULL; newpath=NULL; return 0;}
int atoi(const char *nptr) { nptr=NULL; return 0;}
int creat(const char *a, int b) { a=NULL; b=0; return 0;}
//int erase_char() { return 0;}
int errno;
void error(const char *a, ...) {a=NULL;};
int fclose(void *stream) { stream=NULL; return 0;}
//int fflush() { return 0;}
int fgetc(void *stream) { stream=NULL; return 0;}
char *fgets(char *s, int size, void *stream) { s=NULL; size=0; stream=NULL; return NULL;}
void *fopen (const char *path, const char *mode) {path=NULL; mode=NULL; return NULL;}
int fprintf(void *stream, const char *format, ...) {stream=NULL; format=NULL; return 0;}
//int fputc() { return 0;}
//int fputs() { return 0;}
int fread( void *ptr, int size, int nmemb, void *stream) {ptr=NULL; size=0; nmemb=0; stream=NULL; return 0;}
int fscanf(void *stream, const char *format, ...) {stream=NULL; format=NULL; return 0;}
int fseek(void *stream, long offset, int whence) {stream=NULL; offset=0; whence=0; return 0;}
long ftell(void *stream) {stream=NULL; return 0;}
//int getc() { return 0;}
char *getenv(const char *a) {a=NULL; return NULL;}
//int getuid() { return 0;}
//int hackpid;
//const char *hname;
char *index(const char *s, int c) {s=NULL; c=0; return NULL;}
//int kill_char() { return 0;}
int lseek(int fildes, int offset, int whence) {fildes=0; offset=0; whence=0; return 0;}
//int ospeed() { return 0;}
//int perror() { return 0;}
void qsort(void *base, int nmemb, int size,int (*compar)(const void *, const void *)) {base=NULL; nmemb=0; size=0; compar=NULL;}
int read(int fd, void *buf, int count) {fd=0; buf=NULL; count=0; return 0;}
void rewind(void *stream) { stream=NULL;}
char *rindex(const char *s, int c) {s=NULL; c=0; return NULL;}
//int settty() { return 0;}
int sscanf( const char *str, const char *format, ...) {str=NULL; format=NULL; return 0; }
//int system() { return 0;}
//int tgetch() { return 0;}
//int tgetent() { return 0;}
//int tgetflag() { return 0;}
//int tgetnum() { return 0;}
//int tgetstr() { return 0;}
//int tgoto() { return 0;}
//int tputs() { return 0;}
//int ungetc() { return 0;}
int unlink(const char *pathname) {pathname=NULL; return 0;}
int write(int fd, const void *buf, int count) {fd=0; buf=NULL; count=0; return 0;}


void
glo(foo)
register int foo;
{
	/* construct the string  xlock.n  */
	register char *tf;

	tf = lock;
	while(*tf && *tf != '.') tf++;
	Sprintf(tf, ".%d", foo);
}

#if 0
void setrandom()
{
    SysRandom( TimGetTicks() );
}
#endif

/* FIXME: This was hacked to compile.. it certainly does *not* work */
static struct tm *getlt()
{
    struct tm *date;

    UInt16 seconds, current_days, base_days;
    DateTimeType palm_time;
    DateType palm_date;

	date=NULL;

    /* Get number of seconds since 1/1/1904, and use this number to get the
    time and date values we need. */
    
    seconds = TimGetSeconds();
    TimSecondsToDateTime( seconds, &palm_time );
    
    /* Get the number of days since 1/1/1904, for today and Jan 1st, then
    get the difference to find number of days since Jan 1st. */
    
    DateSecondsToDate( seconds, &palm_date );
    current_days = DateToDays( palm_date );
    palm_date.month = 1;
    palm_date.day = 1;
    base_days = DateToDays( palm_date );
    
    date->tm_sec = palm_time.second;
    date->tm_min = palm_time.minute;
    date->tm_hour = palm_time.hour;
    date->tm_mday = palm_time.day;
    date->tm_mon = palm_time.month - 1;
    date->tm_year = palm_time.year - 1900;
    date->tm_wday = palm_time.weekDay;
    date->tm_yday = current_days - base_days;
    date->tm_isdst = -1; /* How do we check for DST on a Palm device? */
    
    return date;
}

#if 0
int getyear()
{
    return(1900 + getlt()->tm_year);
}
#endif

char *getdate()
{
#ifdef LINT	/* static char datestr[7]; */
    char datestr[7];
#else
    static char datestr[7];
#endif
    register struct tm *lt = getlt();

    Sprintf(datestr, "%2d%2d%2d",
    lt->tm_year, lt->tm_mon + 1, lt->tm_mday);
    if(datestr[2] == ' ') datestr[2] = '0';
    if(datestr[4] == ' ') datestr[4] = '0';
    return(datestr);
}

#if 0
int phase_of_the_moon()			/* 0-7, with 0: new, 4: full */
{					/* moon period: 29.5306 days */
					/* year: 365.2422 days */
    register struct tm *lt = getlt();
    register int epact, diy, goldn;

    diy = lt->tm_yday;
    goldn = (lt->tm_year % 19) + 1;
    epact = (11 * goldn + 18) % 30;
    if ((epact == 25 && goldn > 11) || epact == 24)
        epact++;

    return( (((((diy + epact) * 6) + 11) % 177) / 22) & 7 );
}

int night()
{
    register int hour = getlt()->tm_hour;

    return(hour < 6 || hour > 21);
}

int midnight()
{
    return(getlt()->tm_hour == 0);
}
#endif

/* On UNIX, the 2 functions below do a "stat" on a given file to make certain
it can be accessed.  It appears to be stubbed-out for other platforms since
stat and fstat are UNIX functions, so I'm stubbing them out for the Palm port
as well. */

void gethdate(const char *name)
{
	name=NULL;
}

#if 0
boolean uptodate(int fd, const char *a)
{
	fd=0;
	a=NULL;
    return(1);
}
#endif

/* Regularize is used to take illegal characters out of filenames.  I'm
not sure what would qualify on the Palm, so I'm using the MSDOS rules for
now */

void regularize(s) register char *s;
{
    register char *lp;

    for (lp = s; *lp; lp++)
        if (*lp <= ' ' || *lp == '"' || (*lp >= '*' && *lp <= ',') ||
            *lp == '.' || *lp == '/' || (*lp >= ':' && *lp <= '?') ||
            *lp == '|' || *lp >= 127 || (*lp >= '[' && *lp <= ']'))
            *lp = '_';
}
