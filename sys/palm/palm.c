/* palm.c:  Copyright 2000 by Karl Garrison <kdgarrison@home.com>.  This
file may be distributed under the terms of the Nethack license. */

#include "hack.h"
#include "palmconf.h"
#include "../sys/palm/palmresource.h"

UInt32 PilotMain(UInt16 cmd, void* cmdPBP, UInt16 launchFlags) SEC45;
static void EventLoop() SEC45;
static Boolean ApplicationHandleEvent( EventPtr event ) SEC45;
static Err StartApplication() SEC45;
static void StopApplication() SEC45;
static Boolean MainFormHandleEvent( EventPtr event ) SEC45;
void palm_Game_About() SEC45;


UInt32 PilotMain(UInt16 launchCode, void* cmdPBP, UInt16 launchFlags)
{
    Err err;

    if( launchCode == sysAppLaunchCmdNormalLaunch )
    {
        if( ( err = StartApplication() ) == 0 )
        {
            EventLoop();
            StopApplication();
        }
    }
    return err;

}//PilotMain

/*
 *  A basic event loop
 */

static void EventLoop()
{
    EventType event;
    UInt16 e_error;

    do
    {
        EvtGetEvent( &event, evtWaitForever );
        
        if( !SysHandleEvent( &event ) )
            if( !MenuHandleEvent( 0, &event, &e_error ) )
                if( !ApplicationHandleEvent( &event ) )
                    FrmDispatchEvent( &event );
    } while ( event.eType != appStopEvent );
}//EventLoop

/*
 *  Just load the main form
 */

static Boolean ApplicationHandleEvent( EventPtr event )
{
    FormPtr frm;
    Int16 formId;
    Boolean handled = false;

    if( event->eType == frmLoadEvent )
    {

        formId = event->data.frmLoad.formID;
        frm = FrmInitForm( event->data.frmLoad.formID );
        FrmSetActiveForm( frm );

        switch( formId )
        {
            case Main_Form:
                FrmSetEventHandler( frm, MainFormHandleEvent );
                break;
        }

        handled = true;
    }

    return handled;
}//ApplicationHandleEvent

static Err StartApplication()
{
    FrmGotoForm( Main_Form );
    return 0;
}


/*
 *  Shut down by saving all forms
 */

static void StopApplication()
{
    FrmSaveAllForms();  /* Sends events directly to the form handler */
}


/*
 *  Handle events from the main form
 */

static Boolean MainFormHandleEvent( EventPtr event )
{
    Boolean handled = false;
    int goAgain = 0;


    switch( event->eType )
    {
        case frmOpenEvent:
            FrmDrawForm( FrmGetActiveForm() );
            handled = true;
            break;

    case menuEvent:
        switch( event->data.menu.itemID )
        {
            case Game_Help:
                //palm_Game_Help();
				palm_Game_About();
                break;
        }
        handled = true;
        break;

    case keyDownEvent:
        switch( event->data.keyDown.chr )
        {
            case 'v':
                palm_Game_About();
                break;
			default:
				break;
        }
        handled = true;
        break;
    }

    return handled;
}

void palm_Game_About()
{
    FrmAlert( About_Alert );
}









/* The low-level unbuffered I/O funcions use integers as file descriptors.
The PalmOS File Streaming API uses filehandles.  This array is used to keep
track of what integer corresponds to what file handle. */

//FileHand fd_to_handle[ 20 ];

//int next_fd = 0;

//int PilotMain() {}

#if 0
int palm_open(const char *filename, int mode)
{
    UInt32 palm_mode = 0;
    Char palm_filename[ 32 ];
    
//    if(next_fd == 20) /* Too many open files; artificial limit */
//        panic( "Too many open files!" );
    
    if(mode >= 8) /* Truncate mode */
    {
        palm_mode = fileModeReadWrite;
        mode -= 8;  /* I know bit-shifting would probably look prettier here */
    }
    
    if(mode >= 4)   /* Create file, but don't overwrie */
    {
        palm_mode = fileModeUpdate;
        mode -= 4;
    }
    
    /* If we get this far, the mode must be either write-only or read-only.
    The PalmOS File Streaming API doesn't have a write-only mode, so we set it
    to read-write instead. */
    
    if((mode == 1) && (palm_mode == 0))
        palm_mode = fileModeUpdate;
        
    if((mode == 0) && (palm_mode == 0)) /* Read-only mode */
        palm_mode = fileModeReadOnly;
    
    StrCopy(palm_filename, filename);
    
    fd_to_handle[next_fd] = FileOpen (0, palm_filename, 0, 0, palm_mode, NULL);
    
//    if(fd_to_handle[next_fd] == 0)  /* Some kind of error occured */
//        panic( "File I/O error" );  /* I'll add to this later */
    
    next_fd++;
    
    return next_fd - 1;
}

/* palm_creat is almost identical to palm_open, except that by default the file
is created if it doesn't exist, discarding any previous version of the file.
Technically, creat can take the same file mode parameters as open, but this
is not done anywhere in the Nethack code.  I am including the file mode code
simply for completeness. */

int palm_creat(const char *filename, int mode)
{
    UInt32 palm_mode = 0;
    Char palm_filename[ 32 ];
    
//    if( next_fd == 20 ) /* Too many open files; artificial limit */
//        panic( "Too many open files!" );
    
    if(mode >= 8) /* Truncate mode */
    {
        palm_mode = fileModeReadWrite;
        mode -= 8;  /* I know bit-shifting would probably look prettier here */
    }
    
    if(mode >= 4)   /* Create file, but don't overwrie */
    {
        palm_mode = fileModeUpdate;
        mode -= 4;
    }
    
    /* If we get this far, the mode must be either write-only or read-only.
    The PalmOS File Streaming API doesn't have a write-only mode, so we set it
    to read-write instead. */
    
    if((mode == 1) && (palm_mode == 0))
        palm_mode = fileModeUpdate;
        
    if((mode == 0) && (palm_mode == 0)) /* Normal case */
        palm_mode = fileModeReadWrite;

    StrCopy(palm_filename, filename);
    
    fd_to_handle[next_fd] = FileOpen (0, palm_filename, 0, 0, palm_mode, NULL);
    
//    if(fd_to_handle[next_fd] == 0)  /* Some kind of error occured */
//        panic( "File I/O error" );  /* I'll add to this later */
    
    next_fd++;
    
    return next_fd - 1;
}

int palm_close(int fd)
{
    int count;
    
    if( FileClose(fd_to_handle[fd]) != 0 )
        return -1;
    
    for(count = fd; count < (next_fd - 1); count++)
           fd_to_handle[count] = fd_to_handle[count + 1];
           
    next_fd--;
    
//    if( next_fd < 0 )   /* Should be impossible */
//        panic("error with file pointer array in palm.c");
    
    return 0;
}
#endif


